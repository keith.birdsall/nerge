import pynetbox
from anac import api
from django.conf import settings


class Client:
    def __init__(self):
        self.url = settings.NB_URL
        self.token = settings.NB_TOKEN
        self.conn = pynetbox.api(self.url, token=self.token)

    def get_devices(self, *args, **kwargs):
        return self.conn.dcim.devices.get(*args, **kwargs)

    def filter_devices(self, *args, **kwargs):
        return self.conn.dcim.devices.filter(*args, **kwargs)

    def create_device(self, device):
        return self.conn.dcim.devices.create(device)

    def create_devices(self, devices):
        resp = []
        for device in devices:
            devices[device]["name"] = device
            self.conn.dcim.devices.create(devices[device])
            resp.append(device)
        return resp

    def create_device_types(self, device_types, manufacturers):
        resp = []
        for i, device_type in enumerate(device_types):
            device_params = {
                "name": device_type,
                "slug": device_type,
                "manufacturer": {"slug": manufacturers[i]},
                "model": manufacturers[i],
            }
            self.conn.dcim.device_types.create(device_params)
            resp.append(device_type)
        return resp

    def create_device_roles(self, device_roles):
        resp = []
        for device_role in device_roles:
            self.conn.dcim.device_roles.create(
                {"name": device_role, "slug": device_role}
            )
            resp.append(device_role)
        return resp

    def create_manufacturers(self, manufacturers):
        resp = []
        for manufacturer in manufacturers:
            self.conn.dcim.manufacturers.create(
                {"name": manufacturer, "slug": manufacturer}
            )
            resp.append(manufacturer)
        return resp

    def create_platforms(self, platforms):
        resp = []
        for platform in platforms:
            self.conn.dcim.platforms.create(
                {"name": platform, "slug": platform, "napalm_driver": platform}
            )
            resp.append(platform)
        return resp


class AsyncClient:
    def __init__(self):
        self._client = api(
            settings.NB_URL,
            token=settings.NB_TOKEN,
        )
