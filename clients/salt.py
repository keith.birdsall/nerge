import clients.netbox
import devices.models
import asyncio
import json
import logging
import subprocess
from datetime import datetime
from django.conf import settings

logger = logging.getLogger(__name__)


class Client:
    def __init__(self, return_exceptions=True):
        self.cmd = None
        self.return_exceptions = return_exceptions

    def list_keys(self):
        self.cmd = f"{settings.SALT_DEV_CMD_PREFIX}salt-key --list all"
        result = subprocess.run(
            self.cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
        )
        stdout = result.stdout.decode("utf-8")
        stderr = result.stderr
        if not self.return_exceptions and stderr:
            raise stderr

        result_dict = {"_stdout": stdout, "stderr": stderr.decode("utf-8")}
        key_list = stdout.strip().split("\n")
        for line in key_list:
            if line.endswith(":"):
                key = line[:-1]
                result_dict[key] = []
                continue
            try:
                if key:
                    result_dict[key].append(line)
            except UnboundLocalError as e:
                logger.error(e)
        return result_dict

    def process_device(self, key):
        accepted_key = None
        device_data = {"name": key, "salt_key_status": "unaccepted", "serial": key}
        if settings.NETBOX_INVENTORY_PLUGIN:
            client = clients.netbox.Client()
            netbox_device = client.get_devices(serial=key)
            if netbox_device:
                device_data["name"] = netbox_device.name
                custom_fields = netbox_device.custom_fields
                nsp_network = custom_fields.get("nsp_network")
                if nsp_network:
                    device_data["network"] = nsp_network
                    device_data["salt_key_status"] = "accepted"
                    accepted_key = self.accept_key(key)
        try:
            device = devices.models.BaseNetworkDevice.objects.get(name=key)
        except devices.models.BaseNetworkDevice.DoesNotExist as e:
            log_ctx = {"context": {"device_data": device_data, "error": e}}
            logger.info(f"creating initial device object", extra=log_ctx)
            device = devices.models.BaseNetworkDevice(**device_data)
            device.save()
        return accepted_key

    def accept_key(self, key):
        self.cmd = f"{settings.SALT_DEV_CMD_PREFIX}salt-key -y --accept={key}"
        result = subprocess.run(
            self.cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
        )
        return result

    def delete_key(self, key):
        self.cmd = f"{settings.SALT_DEV_CMD_PREFIX}salt-key -y --delete={key}"
        result = subprocess.run(
            self.cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
        )
        return result

    def reject_key(self, key):
        self.cmd = f"{settings.SALT_DEV_CMD_PREFIX}salt-key -y --reject={key}"
        result = subprocess.run(
            self.cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
        )
        return result

    def regenerate_key(self, key):
        self.cmd = f"{settings.SALT_DEV_CMD_PREFIX}salt '{key}' saltutil.regen_keys"
        result = subprocess.run(
            self.cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
        )
        return result

    def cp_merge_candidate(self, device, **config):
        merge_timestamp = datetime.now().strftime("%Y_%m_%d-%I_%M_%S_%p")
        tmp_file_path = f"/tmp/{merge_timestamp}.json"
        self.cmd = f"{settings.SALT_DEV_CMD_PREFIX}salt-cp '{device}' {tmp_file_path} {tmp_file_path}"
        with open(tmp_file_path, "w") as merge_candidate:
            json.dump(config, merge_candidate)
        cp_merge_candidate_result = subprocess.run(
            self.cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
        )
        cp_merge_candidate_stdout = cp_merge_candidate_result.stdout.decode("utf-8")
        cp_merge_candidate_stderr = cp_merge_candidate_result.stderr
        if not self.return_exceptions and cp_merge_candidate_stderr:
            raise cp_merge_candidate_stderr
        results = {
            "cmd": self.cmd,
            "stdout": cp_merge_candidate_stderr,
            "stderr": cp_merge_candidate_stdout,
            "tmp_file_path": tmp_file_path,
        }
        logger.info(
            f"copied file {tmp_file_path} for merge config",
            extra=results,
        )
        return results

    def snap_config(self, device):
        self.cmd = f"sudo {settings.SALT_DEV_CMD_PREFIX}salt {device} cmd.run 'show runningconfiguration all'"
        cp_snap_config_result = subprocess.run(
            self.cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
        )
        cp_snap_config_stdout = cp_snap_config_result.stdout.decode("utf-8")
        cp_snap_config_stderr = cp_snap_config_result.stderr
        if not self.return_exceptions and cp_snap_config_stderr:
            logger.error(
                f"SaltStack command failed: {self.cmd}",
            )
            raise Exception(f"SaltStack command failed: {cp_snap_config_stderr}")
        logger.info(
            f"File copied from {self.cmd} for rollback config",
        )
        return cp_snap_config_stdout

    def merge_config(self, device, config_path):
        #cmd_prefix = "sudo" if settings.ENVIRONMENT != "development" else ""
        cmd_prefix = "" if settings.ENVIRONMENT != "development" else ""
        cmd = f"{cmd_prefix}config load {config_path} -y"
        self.cmd = f"{settings.SALT_DEV_CMD_PREFIX}salt '{device}' cmd.run '{cmd}'"
        merge_config_result = subprocess.run(
            self.cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
        )
        merge_config_stderr = merge_config_result.stderr
        if not self.return_exceptions and merge_config_stderr:
            raise merge_config_stderr
        return merge_config_result

    def exec_config(self, device, cmd):
        #cmd_prefix = "sudo " if settings.ENVIRONMENT != "development" else ""
        cmd_prefix = "" if settings.ENVIRONMENT != "development" else ""
        self.cmd = (
            f"{settings.SALT_DEV_CMD_PREFIX}salt '{device}' cmd.run '{cmd_prefix}{cmd}'"
        )
        exec_config_result = subprocess.run(
            self.cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
        )
        exec_config_stderr = exec_config_result.stderr
        if not self.return_exceptions and exec_config_stderr:
            raise exec_config_stderr
        if "show" in cmd:
            results = exec_config_result.stdout
            return results
        return exec_config_result

    def save_config(self, device_name):
        #cmd_prefix = "sudo " if settings.ENVIRONMENT != "development" else ""
        cmd_prefix = "" if settings.ENVIRONMENT != "development" else ""
        cmd = f"{cmd_prefix}config save -y"
        self.cmd = f"{settings.SALT_DEV_CMD_PREFIX}salt '{device_name}' cmd.run '{cmd}'"
        save_config_result = subprocess.run(
            self.cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
        )
        save_config_stderr = save_config_result.stderr
        if not self.return_exceptions and save_config_stderr:
            raise save_config_stderr
        return save_config_result


class AsyncClient:
    def __init__(self):
        self.cmd = None

    async def list_keys(self):
        self.cmd = f"{settings.SALT_DEV_CMD_PREFIX}salt-key --list all"
        proc = await asyncio.create_subprocess_exec(
            *self.cmd.split(" "),
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        stdout, stderr = await proc.communicate()
        result_dict = {}
        if stderr:
            result_dict["stderr"] = stderr
        key_list = stdout.decode("utf-8").strip().split("\n")
        for line in key_list:
            if line.endswith(":"):
                key = line[:-1]
                result_dict[key] = []
                continue
            if key:
                result_dict[key].append(line)
        return result_dict

    async def accept_key(self, key):
        self.cmd = f"{settings.SALT_DEV_CMD_PREFIX}salt-key -y --accept={key}"
        proc = await asyncio.create_subprocess_exec(
            *self.cmd.split(" "),
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        stdout, stderr = await proc.communicate()
        return stdout, stderr

    async def reject_key(self, key):
        self.cmd = f"{settings.SALT_DEV_CMD_PREFIX}salt-key -y --reject={key}"
        proc = await asyncio.create_subprocess_exec(
            *self.cmd.split(" "),
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        stdout, stderr = await proc.communicate()
        return stdout, stderr
