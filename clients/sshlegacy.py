import nmap

def scan_subnet(subnet):
    scanner = nmap.PortScanner()
    scanner.scan(hosts=subnet, arguments='-p 22 -O')  # Added the '-O' option for OS fingerprinting

    for host in scanner.all_hosts():
        if 'tcp' in scanner[host]:
            if 22 in scanner[host]['tcp']:
                print(f"Host {host} has an open SSH port (22).")

                if 'osclass' in scanner[host]:
                    for osclass in scanner[host]['osclass']:
                        print(f"  OS Name: {osclass['osfamily']} {osclass['osgen']}")
                        print(f"  OS Accuracy: {osclass['accuracy']}")

if __name__ == "__main__":
    subnet = "192.168.4.0/24"  # Change this to your desired subnet
    scan_subnet(subnet)