from django.core.management.base import BaseCommand
import clients.netbox
from django.conf import settings


def init_netbox() -> None:
    client = clients.netbox.Client()
    site_args: dict = {
        "name": settings.NB_INITIAL_SITE,
        "slug": settings.NB_INITIAL_SITE,
    }
    site = client.conn.dcim.sites.create(site_args)
    print(f"site: {site} created")

    for field in settings.NB_CUSTOM_FIELDS:
        custom_field = client.conn.extras.custom_fields.create(**field)
        print(f"custom_fields: {str(custom_field)} created")

    tenant_args = site_args
    tenant_args["custom_fields"] = {"nsp_network": "osn-poc"}

    tenant = client.conn.tenancy.tenants.create(tenant_args)
    print(f"site: {tenant} created")

    tags = client.conn.extras.tags.create(settings.NB_TAGS)
    print(f"tags: {str(tags)} created")

    provider_args: dict = {
        "name": settings.NB_CIRCUIT_PROVIDER_NAME,
        "slug": settings.NB_CIRCUIT_PROVIDER_NAME,
        "asn": int(settings.NB_CIRCUIT_PROVIDER_ASN),
        "account": settings.NB_CIRCUIT_PROVIDER_ACCOUNT,
        "portal_url": settings.NB_CIRCUIT_PROVIDER_PORTAL_URL,
        "noc_contact": settings.NB_CIRCUIT_PROVIDER_NOC_CONTACT,
        "admin_contact": settings.NB_CIRCUIT_PROVIDER_NOC_CONTACT,
        "comments": settings.NB_CIRCUIT_PROVIDER_COMMENTS,
        "tags": [settings.NB_TAGS],
    }
    provider = client.conn.circuits.providers.create(provider_args)
    print(f"circuit provider: {provider} created")

    device_roles = client.create_device_roles(settings.NB_DEVICE_ROLES)
    print(f"device roles: {str(device_roles)} created")

    manufacturers = client.create_manufacturers(settings.NB_MANUFACTURERS)
    print(f"device manufacturers: {str(manufacturers)} created")

    device_types = client.create_device_types(
        settings.NB_DEVICE_TYPES, settings.NB_MANUFACTURERS
    )
    print(f"device types: {str(device_types)} created")

    platforms = client.create_platforms(settings.NB_PLATFORMS)
    print(f"device platforms: {str(platforms)} created")

    ip_args: dict = {
        "address": "10.0.254.202/24",
        "tenant": {"slug": settings.NB_INITIAL_SITE},
        "device": {"slug": "spine02"},
        "interface": {"name": "spine02-mgmt"},
    }
    ip_address = client.conn.ipam.ip_addresses.create(ip_args)
    print(f"ip_address: {ip_address} created")

    devices = client.create_devices(settings.NB_DEVICE_MAP)
    print(f"devices: {str(devices)} created")

    interface = client.conn.dcim.interfaces.create(
        [
            {"device": {"name": "spine01"}, "type": "virtual", "name": "spine01-mgmt"},
            {"device": {"name": "spine02"}, "type": "virtual", "name": "spine02-mgmt"},
            {
                "device": {"name": "leaf01"},
                "type": "40gbase-x-qsfpp",
                "name": "leaf01-gig0/0",
            },
            {
                "device": {"name": "leaf02"},
                "type": "40gbase-x-qsfpp",
                "name": "leaf02-gig0/0",
            },
            {
                "device": {"name": "leaf01"},
                "type": "40gbase-x-qsfpp",
                "name": "leaf01-gig0/1",
            },
            {
                "device": {"name": "leaf02"},
                "type": "40gbase-x-qsfpp",
                "name": "leaf02-gig0/1",
            },
            {
                "device": {"name": "leaf01"},
                "type": "40gbase-x-qsfpp",
                "name": "leaf01-gig0/2",
            },
            {
                "device": {"name": "leaf02"},
                "type": "40gbase-x-qsfpp",
                "name": "leaf02-gig0/2",
            },
            {
                "device": {"name": "spine01"},
                "type": "40gbase-x-qsfpp",
                "name": "spine01-gig0/1",
            },
            {
                "device": {"name": "spine02"},
                "type": "40gbase-x-qsfpp",
                "name": "spine02-gig0/1",
            },
            {
                "device": {"name": "spine01"},
                "type": "40gbase-x-qsfpp",
                "name": "spine01-gig0/2",
            },
            {
                "device": {"name": "spine02"},
                "type": "40gbase-x-qsfpp",
                "name": "spine02-gig0/2",
            },
        ]
    )
    print(f"interface: {interface} created")


def destroy_netbox() -> None:
    client = clients.netbox.Client()
    devices = client.conn.dcim.devices.all()
    for device in devices:
        print(f"removing device: {device}")
        device.delete()

    site = client.conn.dcim.sites.get(name=settings.NB_INITIAL_SITE)
    if site:
        print(f"removing site: {str(site)}")
        site.delete()

    tags = client.conn.extras.tags.all()
    for tag in tags:
        print(f"removing tag: {str(tag)}")
        tag.delete()

    custom_fields = client.conn.extras.custom_fields.all()
    for field in custom_fields:
        print(f"removing custom field: {str(field)}")
        field.delete()

    providers = client.conn.circuits.providers.all()
    for provider in providers:
        print(f"removing circuit provider: {provider}")
        provider.delete()

    device_roles = client.conn.dcim.device_roles.all()
    for device_role in device_roles:
        print(f"removing device role: {device_role}")
        device_role.delete()

    device_types = client.conn.dcim.device_types.all()
    for device_type in device_types:
        print(f"removing device type: {device_type}")
        device_type.delete()

    platforms = client.conn.dcim.platforms.all()
    for platform in platforms:
        print(f"removing device platform: {platform}")
        platform.delete()

    manufacturers = client.conn.dcim.manufacturers.all()
    for manufacturer in manufacturers:
        print(f"removing device manufacturer: {manufacturer}")
        manufacturer.delete()

    ip_addresses = client.conn.ipam.ip_addresses.all()
    for ip_address in ip_addresses:
        print(f"removing device ip address: {ip_address}")
        ip_address.delete()

    tenants = client.conn.tenancy.tenants.all()
    for tenant in tenants:
        print(f"removing device tenant: {tenant}")
        tenant.delete()


class Command(BaseCommand):
    help = "Mocks netbox resources."

    def add_arguments(self, parser):
        parser.add_argument(
            "--destroy",
            action="store_true",
            help="Removes initial configuration from netbox",
        )
        parser.add_argument(
            "--init",
            action="store_true",
            help="Initializes netbox configuration for test",
        )

    def handle(self, *args, **options):
        if options["destroy"]:
            return destroy_netbox()

        if options["init"]:
            try:
                init_netbox()
            except Exception as e:
                print(e)
