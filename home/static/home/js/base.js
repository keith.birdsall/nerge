/**
 * Sets HTML elements to be highlighted or expanded for the active view.
 * @param {string} djangoView - Name of active django view template.
 */
const setActiveBreadcrumbs = (djangoView) => {
  const breadcrumbs = document.getElementsByClassName("nav-breadcrumb");
  Array.from(breadcrumbs).forEach((breadcrumb) => {
    if (djangoView === breadcrumb.getAttribute("data-tableid")) {
      if (breadcrumb.tagName.toLowerCase() === "button") {
        breadcrumb.classList.remove("collapsed");
        breadcrumb.setAttribute("aria-expanded", "true");
      }
      if (breadcrumb.classList.contains("accordion-nav-body")) {
        breadcrumb.classList.add("accordion-nav-body-active");
      }
      if (breadcrumb.id === `accordion-${djangoView}`) {
        breadcrumb.classList.add("show");
      }
    }
  });
};

/**
 * Global search input.
 */
let searchBarVal = null;

/**
 * Search button object for click event listener.
 */
const searchButton = document.getElementById("searchButton");

/**
 * Search bar object for onsearch event listener.
 */
const searchBar = document.getElementById("searchBar");

/**
 * Global websocket connection.
 */
let ws = null;

/**
 * Config for automatic websocket retries on close.
 */
const wsRetry = { try: 0, attempts: 3 };

/**
 * Connects ws and calls webSocketTable for event handling.
 * @param {string} djangoView - Name of WebSocket consumer for django routing.
 * @param {string} uuid - Optional uuidv4 string to route private channels.
 */
const connectWebSocket = (djangoView, uuid = null) => {
  ws = new WebSocket(
    uuid
      ? `ws://${window.location.host}/ws/${djangoView}/${uuid}/`
      : `ws://${window.location.host}/ws/${djangoView}/`
  );
  webSocketTable(djangoView, uuid);
};

/**
 * Slider element used to toggle websocket connection.
 */
const refreshSwitch = document.getElementById("flexSwitchCheckDefault");

/**
 * Event handling used to draw table changes from websocket messages in HTML.
 * @param {string} djangoView - Name of WebSocket consumer for django routing.
 * @param {string} uuid - Optional uuidv4 string to route private channels.
 */
const webSocketTable = (djangoView, uuid) => {
  ws.onopen = () => {
    refreshSwitch.checked = true;
    refreshSwitch.addEventListener("click", (e) => {
      if (ws?.readyState == WebSocket.OPEN) {
        ws.close();
      }
      if (ws?.readyState == WebSocket.CLOSED) {
        connectWebSocket(djangoView, uuid);
      }
    });
    searchBar.onsearch = () => {
      if (ws?.readyState == WebSocket.OPEN) {
        ws.send(
          JSON.stringify({
            django_model_filter: djangoSearchFilter(),
            django_model_search: true,
          })
        );
      }
    };
    searchButton.addEventListener("click", (e) => {
      if (ws?.readyState == WebSocket.OPEN) {
        ws.send(
          JSON.stringify({
            django_model_filter: djangoSearchFilter(),
            django_model_search: true,
          })
        );
      }
    });
    subscribeToUpdates(1100);
  };

  ws.onmessage = (e) => {
    const data = JSON.parse(e.data);
    const message = data.message;
    const oldVal = data.old_val;
    const subscribeToUpdates = data.subscribe_to_updates;
    if (Array.isArray(message)) {
      if (subscribeToUpdates === undefined) {
        redrawTable(djangoView, message);
      } else {
        if (oldVal && oldVal != message) {
          redrawTable(djangoView, message);
        }
      }
    }
  };

  ws.onclose = (e) => {
    wsRetry.try++;
    if (wsRetry.try <= wsRetry.attempts) {
      connectWebSocket(djangoView, uuid);
    } else {
      refreshSwitch.checked = false;
      console.error("disconnected");
    }
  };
};

/**
 * Generates a uuidv4 string.
 * @return {string} unique string to represent private django channels.
 */
const uuidv4 = () => {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
    (
      c ^
      (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
    ).toString(16)
  );
};

/**
 * Asynchronous sleep function to help with websocket events.
 * @param {number} ms - Time to sleep calling function in event loop.
 */
const sleep = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

/**
 * Recursive function to receive websocket updates on an interval.
 * @param {number} ms - Time interval to wait before requesting model updates.
 */
const subscribeToUpdates = async (ms) => {
  const djangoModelFilter = djangoSearchFilter();
  ws.send(
    JSON.stringify({
      django_model_filter: djangoModelFilter,
      django_model_search: true,
      subscribe_to_updates: true,
    })
  );
  await sleep(ms);
  if (ws?.readyState == WebSocket.OPEN) {
    await subscribeToUpdates(ms);
  }
};

/**
 * Generates the django model filter from search bar value.
 */
const djangoSearchFilter = () => {
  searchBarVal = searchBar.value;
  const djangoModelFilter = searchBarVal
    .split(",")
    .reduce((obj, str, index) => {
      const strParts = str.split(":");
      if (strParts[0] && strParts[1]) {
        obj[strParts[0].replace(/\s+/g, "")] = strParts[1].trim();
      }
      return obj;
    }, {});
  return djangoModelFilter;
};

/**
 * Display map for table data containing application name keys for template data values.
 */
const DISPLAY_MAP = {
  dashboards: {},
  devices: {
    salt_key_status: `<span class="{{text_class}}">{{salt_key_status}}</span>{{button}}`,
    network: `<span contenteditable="true" class="network-field">{{network}}</span>`,
    templateKeys: ["salt_key_status", "name", "network"],
    evalMap: {
      [`item.salt_key_status === "unaccepted"`]: {
        text_class: "text-info",
        button: `<button class="btn btn-outline-success float-end" data-bs-toggle="modal" data-bs-target="#saltKeyModal" data-salt-key-title="Accept Key" data-salt-key-body='Are you sure you want to accept: ' data-device-id="{{name}}" data-salt-key-button="Accept" data-salt-key-url="/accept/{{name}}"><i class="fa-solid fa-check"></i></button><button class="btn btn-outline-danger float-end" data-bs-toggle="modal" data-bs-target="#saltKeyModal" data-salt-key-title="Reject Key" data-salt-key-body='Are you sure you want to reject: ' data-device-id="{{name}}" data-salt-key-button="Reject" data-salt-key-url="/reject/{{name}}"><i class="fa-solid fa-xmark"></i></button>`,
      },
      [`item.salt_key_status === "accepted"`]: {
        text_class: "text-success",
        button: "",
      },
      [`item.salt_key_status === "denied"`]: {
        text_class: "text-warning",
        button: `<button class="btn btn-outline-success float-end" data-bs-toggle="modal" data-bs-target="#saltKeyModal" data-salt-key-title="Accept Key" data-salt-key-body='Are you sure you want to accept: ' data-device-id="{{name}}" data-salt-key-button="Accept" data-salt-key-url="/accept/{{name}}"><i class="fa-solid fa-check"></i></button><button class="btn btn-outline-danger float-end" data-bs-toggle="modal" data-bs-target="#saltKeyModal" data-salt-key-title="Reject Key" data-salt-key-body='Are you sure you want to reject: ' data-device-id="{{name}}" data-salt-key-button="Reject" data-salt-key-url="/reject/{{name}}"><i class="fa-solid fa-xmark"></i></button>`,
      },
      [`item.salt_key_status === "rejected"`]: {
        text_class: "text-danger",
        button: `<button class="btn btn-outline-success float-end" data-bs-toggle="modal" data-bs-target="#saltKeyModal" data-salt-key-title="Accept Key" data-salt-key-body='Are you sure you want to accept: ' data-device-id="{{name}}" data-salt-key-button="Accept" data-salt-key-url="/accept/{{name}}"><i class="fa-solid fa-check"></i></button><button class="btn btn-outline-danger float-end" data-bs-toggle="modal" data-bs-target="#saltKeyModal" data-salt-key-title="Reject Key" data-salt-key-body='Are you sure you want to reject: ' data-device-id="{{name}}" data-salt-key-button="Reject" data-salt-key-url="/reject/{{name}}"><i class="fa-solid fa-xmark"></i></button>`,
      },
    },
  },
  jobs: {
    templateKeys: ["id"],
    id: `<div class="clickable" data-tableid="jobs"><span><a href="/jobs/executions/job/{{id}}">{{id}}</a></span></div>`,
  },
  executions: {
    templateKeys: ["id", "job_id"],
    id: `<div class="clickable" data-tableid="results"><span><a href="/jobs/results/id/{{id}}">{{id}}</a></span></div>`,
    job_id: `<div class="clickable" data-tableid="results"><span><a href="/jobs/results/job/{{job_id}}">{{job_id}}</a></span></div>`,
  },
  results: {
    templateKeys: ["job_execution_id", "job_id"],
    job_execution_id: `<div class="clickable" data-tableid="results"><span><a href="/jobs/results/id/{{job_execution_id}}">{{job_execution_id}}</a></span></div>`,
    job_id: `<div class="clickable" data-tableid="results"><span><a href="/jobs/results/job/{{job_id}}">{{job_id}}</a></span></div>`,
  },
  networks: {
    templateKeys: ["name", "type"],
    name: `<div class="clickable" data-tableid="networks"><span><a href="/networks/name/{{name}}">{{name}}</a></span></div>`,
    type: `<div class="clickable" data-tableid="networks"><span><a href="/networks/type/{{type}}">{{type}}</a></span></div>`,
  },
};

/**
 * Redraws HTML table from dictionary object using keys as column headers and values as rows.
 * @param {string} tableId - ID of HTML table element.
 * @param {Array<object>} data - Data to be written as HTML table.
 */
const redrawTable = (tableId, data) => {
  const table = document.getElementById(tableId);
  const tableHead = document.querySelector(`#${tableId} thead`);
  table.replaceChildren();
  table.appendChild(tableHead);
  const tableBody = table.createTBody();
  const displayMap = DISPLAY_MAP[tableId];
  const templateKeys = displayMap?.templateKeys;
  const evalMap = displayMap?.evalMap;
  for (let i = 0; i < data.length; i += 1) {
    const item = data[i];
    const tableBodyRow = tableBody.insertRow();
    for (let [key, val] of Object.entries(item)) {
      let th = null;
      if (Array.isArray(val) || (typeof val === "object" && val !== null)) {
        val = JSON.stringify(val);
      }
      let innerHTML = null;
      if (displayMap) {
        innerHTML = displayMap[key];
      }
      if (innerHTML !== undefined && templateKeys !== undefined) {
        if (evalMap !== undefined) {
          for (const [exprKey, exprVal] of Object.entries(evalMap)) {
            const conditionalExpr = eval(exprKey);
            if (conditionalExpr === true) {
              for (const [evalKey, evalVal] of Object.entries(exprVal)) {
                innerHTML = innerHTML.replace(
                  new RegExp(`{{${evalKey}}}`, "gi"),
                  evalVal
                );
              }
            }
          }
        }
        templateKeys.forEach((templateKey) => {
          const dataVal = item[templateKey];
          innerHTML = innerHTML.replace(
            new RegExp(`{{${templateKey}}}`, "gi"),
            dataVal
          );
        });
        th = Object.assign(document.createElement("th"), {
          scope: "row",
          innerHTML: innerHTML,
        });
      } else {
        th = Object.assign(document.createElement("th"), {
          scope: "row",
          innerText: val,
        });
      }
      tableBodyRow.appendChild(th);
    }
  }
};
