import networks.models
from django.contrib import admin

admin.site.register(networks.models.BaseNetwork)
admin.site.register(networks.models.Vrf)
admin.site.register(networks.models.L3Vni)
admin.site.register(networks.models.Vlan)
admin.site.register(networks.models.Interface)
#admin.site.register(networks.models.Snapshot)
