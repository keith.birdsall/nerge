import devices.models
import jobs.scheduler
import networks.models
import utils.helpers
from datetime import datetime, time
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
import logging
import time
from django.conf import settings
import jobs

logger = logging.getLogger(__name__)


@receiver(
    post_save,
    sender=networks.models.BaseNetwork,
    dispatch_uid="network_save",
)
def network_save(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("saved network", extra=log_ctx)


@receiver(
    pre_delete,
    sender=networks.models.BaseNetwork,
    dispatch_uid="network_delete",
)
def network_delete(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("deleting network", extra=log_ctx)


@receiver(
    post_save,
    sender=networks.models.BaseNetwork,
    dispatch_uid="standalone_network_save",
)
def standalone_network_save(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("saved standalone network", extra=log_ctx)


@receiver(
    pre_delete,
    sender=networks.models.BaseNetwork,
    dispatch_uid="standalone_network_delete",
)
def standalone_network_delete(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("deleting standalone network", extra=log_ctx)


@receiver(
    post_save,
    sender=networks.models.BridgeDomain,
    dispatch_uid="bridge_domain_save",
)
def bridge_domain_save(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("saved bridge domain", extra=log_ctx)


@receiver(
    pre_delete,
    sender=networks.models.BridgeDomain,
    dispatch_uid="bridge_domain_delete",
)
def bridge_domain_delete(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("deleting bridge domain", extra=log_ctx)


@receiver(
    post_save,
    sender=networks.models.EndpointGroup,
    dispatch_uid="endpoint_group_save",
)
def endpoint_group_save(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("saved endpoint group", extra=log_ctx)


@receiver(
    pre_delete,
    sender=networks.models.EndpointGroup,
    dispatch_uid="endpoint_group_delete",
)
def endpoint_group_delete(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("deleting endpoint group", extra=log_ctx)


@receiver(
    post_save,
    sender=networks.models.Tenant,
    dispatch_uid="tenant_save",
)
def tenant_save(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("saved tenant", extra=log_ctx)


@receiver(
    pre_delete,
    sender=networks.models.Tenant,
    dispatch_uid="tenant_delete",
)
def tenant_delete(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("deleting tenant", extra=log_ctx)


@receiver(
    post_save,
    sender=networks.models.Vrf,
    dispatch_uid="vrf_save",
)
def vrf_save(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("saved vrf", extra=log_ctx)
    vrf_name = instance_dict.get("name")
    network_name = instance_dict.get("network")
    interface = instance_dict.get("interface")
    try:
        network = networks.models.BaseNetwork.objects.get(name=network_name)
        if not network.type == "standalone":
            raise NotImplementedError(
                "only network type standalone has a vrf save receiver"
            )
        try:
            device = devices.models.BaseNetworkDevice.objects.get(network=network_name)
            device_name = device.name
            if device_name == "leaf01":
                interface = "Ethernet8"
            device_config = {
                "INTERFACE": {interface: {"vrf_name": vrf_name}},
                "VRF": {vrf_name: {}},
            }
            job = jobs.scheduler.background_scheduler.get_job(
                "sonic_merge_config_vrf_save"
            )
            job.modify(
                next_run_time=datetime.now(),
                args=(device_name, "vrf_save"),
                kwargs=device_config,
            )

        except devices.models.BaseNetworkDevice.DoesNotExist as e:
            log_ctx = {"context": {"error": e}}
            logger.error(
                f"no device belonging to network: '{network_name}'", extra=log_ctx
            )
    except networks.models.BaseNetwork.DoesNotExist as e:
        log_ctx = {"context": {"error": e}}
        logger.error(f"no network with name: '{network_name}'", extra=log_ctx)


@receiver(
    pre_delete,
    sender=networks.models.Vrf,
    dispatch_uid="vrf_delete",
)
def vrf_delete(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("deleting vrf", extra=log_ctx)
    vrf_name = instance_dict.get("name")
    network_name = instance_dict.get("network")
    try:
        network = networks.models.BaseNetwork.objects.get(name=network_name)
        if not network.type == "standalone":
            raise NotImplementedError(
                "only network type standalone has a vrf delete receiver"
            )
        try:
            device = devices.models.BaseNetworkDevice.objects.get(network=network_name)
            device_name = device.name
            job = jobs.scheduler.background_scheduler.get_job(
                "sonic_exec_config_vrf_delete"
            )
            job.modify(
                next_run_time=datetime.now(),
                args=(device_name, "vrf_delete"),
                kwargs={"cmd": f"config vrf del {vrf_name}"},
            )

        except devices.models.BaseNetworkDevice.DoesNotExist as e:
            log_ctx = {"context": {"error": e}}
            logger.error(
                f"no device belonging to network: '{network_name}'", extra=log_ctx
            )
    except networks.models.BaseNetwork.DoesNotExist as e:
        log_ctx = {"context": {"error": e}}
        logger.error(f"no network with name: '{network_name}'", extra=log_ctx)


@receiver(
    post_save,
    sender=networks.models.L3Vni,
    dispatch_uid="l3vni_save",
)
def l3vni_save(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("saving l3vni", extra=log_ctx)


@receiver(
    pre_delete,
    sender=networks.models.L3Vni,
    dispatch_uid="l3vni_delete",
)
def l3vni_delete(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("deleting l3vni", extra=log_ctx)


@receiver(
    post_save,
    sender=networks.models.Interface,
    dispatch_uid="interface_save",
)
def interface_save(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("saved interface", extra=log_ctx)
    network_name = instance_dict.get("network")
    ipv4_address = instance_dict.get("ipv4")
    port = instance_dict.get("port")
    vlan_id = instance_dict.get("vlan")
    tagging_mode = instance_dict.get("tagging_mode")
    try:
        network = networks.models.BaseNetwork.objects.get(name=network_name)
        if not network.type == "standalone":
            raise NotImplementedError(
                "only network type standalone has a interface save receiver"
            )
        try:
            device = devices.models.BaseNetworkDevice.objects.get(network=network_name)
            device_name = device.name
            if device_name == "leaf01":
                port = "Ethernet8"
            if vlan_id:
                device_config = {
                    "VLAN_INTERFACE": {
                        f"Vlan{vlan_id}": {},
                        f"Vlan{vlan_id}|{ipv4_address}": {},
                    },
                    "VLAN_MEMBER": {
                        f"Vlan{vlan_id}|{port}": {"tagging_mode": tagging_mode}
                    },
                    "PORT": {port: {"admin_status": "up"}}
                }
            else:
                device_config = {"INTERFACE": {port: {}, f"{port}|{ipv4_address}": {}}, "PORT": {port: {"admin_status": "up"}}}
            job = jobs.scheduler.background_scheduler.get_job(
                "sonic_merge_config_interface_save"
            )
            job.modify(
                next_run_time=datetime.now(),
                args=(device_name, "interface_save"),
                kwargs=device_config,
            )

        except devices.models.BaseNetworkDevice.DoesNotExist as e:
            log_ctx = {"context": {"error": e}}
            logger.error(
                f"no device belonging to network: '{network_name}'", extra=log_ctx
            )
    except networks.models.BaseNetwork.DoesNotExist as e:
        log_ctx = {"context": {"error": e}}
        logger.error(f"no network with name: '{network_name}'", extra=log_ctx)


@receiver(
    pre_delete,
    sender=networks.models.Interface,
    dispatch_uid="interface_delete",
)
def interface_delete(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("deleting interface", extra=log_ctx)
    network_name = instance_dict.get("network")
    port = instance_dict.get("port")
    ipv4_address = instance_dict.get("ipv4")
    vlan_id = instance_dict.get("vlan")
    try:
        network = networks.models.BaseNetwork.objects.get(name=network_name)
        if not network.type == "standalone":
            raise NotImplementedError(
                "only network type standalone has a interface delete receiver"
            )
        try:
            device = devices.models.BaseNetworkDevice.objects.get(network=network_name)
            device_name = device.name
            if device_name == "leaf01":
                port = "Ethernet8"
            job = jobs.scheduler.background_scheduler.get_job(
                "sonic_exec_config_interface_delete"
            )
            if vlan_id:
                cmd = f"config interface ip remove Vlan{vlan_id} {ipv4_address} && config vlan member del {vlan_id} {port}"
            else:
                cmd = f"config interface ip remove {port} {ipv4_address}"
            job.modify(
                next_run_time=datetime.now(),
                args=(device_name, "interface_delete"),
                kwargs={"cmd": cmd},
            )

        except devices.models.BaseNetworkDevice.DoesNotExist as e:
            log_ctx = {"context": {"error": e}}
            logger.error(
                f"no device belonging to network: '{network_name}'", extra=log_ctx
            )
    except networks.models.BaseNetwork.DoesNotExist as e:
        log_ctx = {"context": {"error": e}}
        logger.error(f"no network with name: '{network_name}'", extra=log_ctx)


@receiver(
    post_save,
    sender=networks.models.Vlan,
    dispatch_uid="vlan_save",
)
def vlan_save(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("saved interface", extra=log_ctx)
    vlan_id = instance_dict.get("vlan")
    network_name = instance_dict.get("network")
    try:
        network = networks.models.BaseNetwork.objects.get(name=network_name)
        if not network.type == "standalone":
            raise NotImplementedError(
                "only network type standalone has a vlan save receiver"
            )
        try:
            # This for loop is used to loop through devices in the network tag and create Unique JOBid
            device = devices.models.BaseNetworkDevice.objects.filter(network=network_name)
            for device_item in device:
                device_name = device_item.name
                device_config = {
                    "VLAN": {f"Vlan{vlan_id}": {"vlanid": f"{vlan_id}"}},
                }
                job_id = f"sonic_merge_config_vlan_save_{device_name}"
                job = jobs.scheduler.background_scheduler.get_job(job_id)
                if job is None:
                    # Create a new job if it doesn't exist within the for loop of device's
                    job = jobs.scheduler.background_scheduler.add_job(
                        func=jobs.scheduler.sonic_merge_config,
                        args=(),
                        kwargs={},
                        trigger="interval",
                        weeks=432,
                        id=job_id,
                        max_instances=settings.JOBS_WORKERS,
                        replace_existing=True,
                        misfire_grace_time=None,
                    )

                job.modify(
                    next_run_time=datetime.now(),
                    args=(device_name, "vlan_save"),
                    kwargs=device_config,
                )

        except devices.models.BaseNetworkDevice.DoesNotExist as e:
            log_ctx = {"context": {"error": e}}
            logger.error(
                f"no device belonging to network: '{network_name}'", extra=log_ctx
            )
    except networks.models.BaseNetwork.DoesNotExist as e:
        log_ctx = {"context": {"error": e}}
        logger.error(f"no network with name: '{network_name}'", extra=log_ctx)


@receiver(
    pre_delete,
    sender=networks.models.Vlan,
    dispatch_uid="vlan_delete",
)
def vlan_delete(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("deleting interface", extra=log_ctx)
    vlan_id = instance_dict.get("vlan")
    network_name = instance_dict.get("network")
    try:
        network = networks.models.BaseNetwork.objects.get(name=network_name)
        if not network.type == "standalone":
            raise NotImplementedError(
                "only network type standalone has a vlan delete receiver"
            )
        try:
            device = devices.models.BaseNetworkDevice.objects.get(network=network_name)
            device_name = device.name
            job = jobs.scheduler.background_scheduler.get_job(
                "sonic_exec_config_vlan_delete"
            )
            job.modify(
                next_run_time=datetime.now(),
                args=(device_name, "vlan_delete"),
                kwargs={"cmd": f"config vlan del {vlan_id}"},
            )

        except devices.models.BaseNetworkDevice.DoesNotExist as e:
            log_ctx = {"context": {"error": e}}
            logger.error(
                f"no device belonging to network: '{network_name}'", extra=log_ctx
            )
    except networks.models.BaseNetwork.DoesNotExist as e:
        log_ctx = {"context": {"error": e}}
        logger.error(f"no network with name: '{network_name}'", extra=log_ctx)
"""
@receiver(
    post_save,
    sender=networks.models.Snapshot,
    dispatch_uid="snapshot_save",
)

def snapshot_save(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    #logger.info("saving snapshot", extra=log_ctx)
    logger.info("saving snapshot")
    network_name = instance_dict.get("network")
    latest_snapshot = Snapshot.objects.latest('id')
    try:
        network = networks.models.BaseNetwork.objects.get(name=network_name)
        if not network.type == "standalone":
            raise NotImplementedError(
                "only network type standalone has a snapshot save receiver"
            )
        try:
            device = devices.models.BaseNetworkDevice.objects.get(network=network_name)
            device_name = device.name
            job = jobs.scheduler.background_scheduler.get_job(
                "snapshot_save_config"
            )
            job.modify(
                next_run_time=datetime.now(),
                args=(device_name, latest_snapshot),
                #kwargs={"cmd": f"cp -f /etc/sonic/config_db.json /tmp/{filename}"},
                kwargs={"cmd": "show runningconfiguration ports"},
            )
            print("THIS IS THE START !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

        except devices.models.BaseNetworkDevice.DoesNotExist as e:
            log_ctx = {"context": {"error": e}}
            logger.error(
                f"no device belonging to network: '{network_name}'", extra=log_ctx
                )
    except networks.models.BaseNetwork.DoesNotExist as e:
        log_ctx = {"context": {"error": e}}
        logger.error(f"no network with name: '{network_name}'", extra=log_ctx)
"""

