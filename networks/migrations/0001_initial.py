# Generated by Django 4.1.11 on 2023-10-12 13:57

import django.contrib.postgres.fields
import django.core.validators
from django.db import migrations, models
import django_prometheus.models


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="BaseNetwork",
            fields=[
                ("id", models.AutoField(primary_key=True, serialize=False)),
                ("name", models.CharField(max_length=63)),
                ("type", models.CharField(blank=True, max_length=50)),
                ("description", models.CharField(blank=True, max_length=200)),
            ],
            bases=(
                django_prometheus.models.ExportModelOperationsMixin("network"),
                models.Model,
            ),
        ),
        migrations.CreateModel(
            name="BridgeDomain",
            fields=[
                ("id", models.AutoField(primary_key=True, serialize=False)),
                (
                    "name",
                    models.CharField(
                        max_length=63,
                        validators=[
                            django.core.validators.RegexValidator(
                                message="name must be in all upper-case letters, and follow this schema:\n                         'Data Center'-'Environment'-'BD'-'vlan tag'",
                                regex="^[A-Z]{3}-[A-Z]{4}-BD-[0-9]{0,4}$",
                            )
                        ],
                    ),
                ),
            ],
            bases=(
                django_prometheus.models.ExportModelOperationsMixin("bridge_domain"),
                models.Model,
            ),
        ),
        migrations.CreateModel(
            name="EndpointGroup",
            fields=[
                ("id", models.AutoField(primary_key=True, serialize=False)),
                (
                    "name",
                    models.CharField(
                        max_length=63,
                        validators=[
                            django.core.validators.RegexValidator(
                                message="end point group_name must be in all upper-case letters, and follow this schema:\n                         'Data Center'-'Environment'-'EPG'-'vlan tag'",
                                regex="^[A-Z]{3}-[A-Z]{4}-EPG-[0-9]{0,4}$",
                            )
                        ],
                    ),
                ),
                (
                    "ports",
                    django.contrib.postgres.fields.ArrayField(
                        base_field=models.CharField(
                            blank=True, max_length=50, null=True
                        ),
                        blank=True,
                        null=True,
                        size=None,
                    ),
                ),
                ("vlan", models.IntegerField(blank=True, null=True)),
                ("vni", models.IntegerField(blank=True, null=True)),
            ],
            bases=(
                django_prometheus.models.ExportModelOperationsMixin("epg"),
                models.Model,
            ),
        ),
        migrations.CreateModel(
            name="Interface",
            fields=[
                ("id", models.AutoField(primary_key=True, serialize=False)),
                ("port", models.CharField(max_length=50)),
                (
                    "vlan",
                    models.IntegerField(
                        blank=True,
                        null=True,
                        validators=[
                            django.core.validators.MinValueValidator(1),
                            django.core.validators.MaxValueValidator(4094),
                        ],
                    ),
                ),
                ("ipv4", models.CharField(blank=True, max_length=50)),
                ("network", models.CharField(blank=True, max_length=63)),
                ("device", models.CharField(blank=True, max_length=50)),
                ("description", models.CharField(blank=True, max_length=200)),
                ("tagging_mode", models.CharField(max_length=50)),
            ],
            bases=(
                django_prometheus.models.ExportModelOperationsMixin("interface"),
                models.Model,
            ),
        ),
        migrations.CreateModel(
            name="L3Vni",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "l3_vni",
                    models.IntegerField(
                        blank=True,
                        null=True,
                        validators=[
                            django.core.validators.MinValueValidator(1),
                            django.core.validators.MaxValueValidator(16777215),
                        ],
                    ),
                ),
            ],
            bases=(
                django_prometheus.models.ExportModelOperationsMixin("l3vni"),
                models.Model,
            ),
        ),
        migrations.CreateModel(
            name="Snapshot",
            fields=[
                ("id", models.AutoField(primary_key=True, serialize=False)),
                ("device", models.CharField(blank=True, max_length=63)),
                ("network", models.CharField(blank=True, max_length=63)),
                ("filename", models.CharField(blank=True, max_length=200)),
                ("path", models.CharField(blank=True, max_length=200)),
            ],
            bases=(
                django_prometheus.models.ExportModelOperationsMixin("snapshot"),
                models.Model,
            ),
        ),
        migrations.CreateModel(
            name="Tenant",
            fields=[
                ("id", models.AutoField(primary_key=True, serialize=False)),
                (
                    "name",
                    models.CharField(
                        max_length=63,
                        validators=[
                            django.core.validators.RegexValidator(
                                message="name must be in all upper-case letters, and follow this schema:\n                         'Data Center'-'Environment'-'IP/INP/EP/ENP'",
                                regex="^[A-Z]{3}-[A-Z]{4}-(IP|INP|EP|ENP)$",
                            )
                        ],
                    ),
                ),
            ],
            bases=(
                django_prometheus.models.ExportModelOperationsMixin("tenant"),
                models.Model,
            ),
        ),
        migrations.CreateModel(
            name="Vlan",
            fields=[
                ("id", models.AutoField(primary_key=True, serialize=False)),
                (
                    "vlan",
                    models.IntegerField(
                        validators=[
                            django.core.validators.MinValueValidator(1),
                            django.core.validators.MaxValueValidator(4094),
                        ]
                    ),
                ),
                ("name", models.CharField(max_length=63)),
                ("network", models.CharField(blank=True, max_length=63)),
                ("description", models.CharField(blank=True, max_length=200)),
            ],
            bases=(
                django_prometheus.models.ExportModelOperationsMixin("vlan"),
                models.Model,
            ),
        ),
        migrations.CreateModel(
            name="Vrf",
            fields=[
                ("id", models.AutoField(primary_key=True, serialize=False)),
                (
                    "name",
                    models.CharField(
                        max_length=63,
                        validators=[
                            django.core.validators.RegexValidator(
                                message="name must be in all upper-case letters, and follow this schema:\n                         'Data Center'-'Environment'-'optional: Area'RDC,ESAE, etc'-'IP/INP/EP/ENP/LAB'-'optional: secure area'",
                                regex="^Vrf-[A-Z]{3}-[A-Z]{4}$",
                            )
                        ],
                    ),
                ),
                ("network", models.CharField(blank=True, max_length=63)),
                ("interface", models.CharField(blank=True, max_length=100)),
            ],
            bases=(
                django_prometheus.models.ExportModelOperationsMixin("vrf"),
                models.Model,
            ),
        ),
    ]
