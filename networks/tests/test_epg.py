import networks.models
from django.core.exceptions import ValidationError
from django.test import TestCase


class EndpointGroupModelTest(TestCase):
    def setUp(self):
        self.valid_epg_name = "PDC-ICCU-EPG-105"
        self.invalid_epg_name = "PDC-ICCU-105-EPG"

    def test_valid_epg_name(self):
        """
        Test that a valid epg name passes validation.
        """
        epg_change = networks.models.EndpointGroup(name=self.valid_epg_name)
        try:
            epg_change.full_clean()
        except ValidationError as e:
            self.fail(f"ValidationError was raised: {e}")

    def test_invalid_epg_name(self):
        """
        Test that an invalid epg name raises a ValidationError.
        """
        epg_change = networks.models.EndpointGroup(name=self.invalid_epg_name)
        with self.assertRaises(ValidationError):
            epg_change.full_clean()
