import networks.models
from django.core.exceptions import ValidationError
from django.test import TestCase


class VrfModelTest(TestCase):
    def setUp(self):
        # self.valid_vrf_name = "PDC-ICCU-IP"
        self.valid_vrf_name = "Vrf-PDC-ICCU"
        # self.valid_3_hyphen_vrf_name = "PDC-ICCU-ESAE-IP"
        self.invalid_vrf_name = "PDC-ICCu-IP"
        # self.invalid_3_hyphen_vrf_name = "PDC-ICCU-ESAES-IP"

    def test_valid_vrf_name(self):
        """
        Test that a valid vrf name passes validation.
        """
        vrf_change = networks.models.Vrf(name=self.valid_vrf_name)
        try:
            vrf_change.full_clean()
        except ValidationError as e:
            self.fail(f"ValidationError was raised: {e}")

    def test_invalid_vrf_name(self):
        """
        Test that an invalid vrf name raises a ValidationError.
        """
        vrf_change = networks.models.Vrf(name=self.invalid_vrf_name)
        with self.assertRaises(ValidationError):
            vrf_change.full_clean()

    # def test_valid_3_hyphen_vrf_name(self):
    #     """
    #     Test that a valid vrf name with optional area passes validation.
    #     """
    #     vrf_change = networks.models.Vrf(name=self.valid_3_hyphen_vrf_name)
    #     try:
    #         vrf_change.full_clean()
    #     except ValidationError as e:
    #         self.fail(f"ValidationError was raised: {e}")

    # def test_invalid_3_hyphen_vrf_name(self):
    #     """
    #     Test that an invalid vrf name with optional area raises a ValidationError.
    #     """
    #     vrf_change = networks.models.Vrf(name=self.invalid_3_hyphen_vrf_name)
    #     with self.assertRaises(ValidationError):
    #         vrf_change.full_clean()
