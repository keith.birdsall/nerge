import networks.models
from django.core.exceptions import ValidationError
from django.test import TestCase


class BridgeDomainModelTest(TestCase):
    def setUp(self):
        self.valid_bd_name = "PDC-ICCU-BD-105"
        self.invalid_bd_name = "PDC-ICCU-105-BD"

    def test_valid_bd_name(self):
        """
        Test that a valid bd name passes validation.
        """
        bd_change = networks.models.BridgeDomain(name=self.valid_bd_name)
        try:
            bd_change.full_clean()
        except ValidationError as e:
            self.fail(f"ValidationError was raised: {e}")

    def test_invalid_bd_name(self):
        """
        Test that an invalid bd name raises a ValidationError.
        """
        bd_change = networks.models.BridgeDomain(name=self.invalid_bd_name)
        with self.assertRaises(ValidationError):
            bd_change.full_clean()
