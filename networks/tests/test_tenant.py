import networks.models
from django.core.exceptions import ValidationError
from django.test import TestCase


class TenantModelTest(TestCase):
    def setUp(self):
        self.valid_tenant_name = "PDC-ICCU-IP"
        self.invalid_tenant_name = "PDC-env-IP"

    def test_valid_tenant_name(self):
        """
        Test that a valid tenant name passes validation.
        """
        tenant_change = networks.models.Tenant(name=self.valid_tenant_name)
        try:
            tenant_change.full_clean()
        except ValidationError as e:
            self.fail(f"ValidationError was raised: {e}")

    def test_invalid_tenant_name(self):
        """
        Test that an invalid tenant name raises a ValidationError.
        """
        tenant_change = networks.models.Tenant(name=self.invalid_tenant_name)
        with self.assertRaises(ValidationError):
            tenant_change.full_clean()
