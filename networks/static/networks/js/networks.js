setActiveBreadcrumbs("networks");
connectWebSocket("networks", uuidv4());
const uri = window.location.toString().split("networks/")[1];
let initialSearchBarValue = null;
if (uri.includes("name/")) {
  initialSearchBarValue = { name: uri.split("name/")[1] };
} else if (uri.includes("type/")) {
  initialSearchBarValue = { type: uri.split("type/")[1] };
} else {
  initialSearchBarValue = "";
}
initialSearchBarValue = Object.keys(initialSearchBarValue)
  .map((key) => `${key}:${initialSearchBarValue[key]}`)
  .join(" ");
searchBar.value = initialSearchBarValue;
