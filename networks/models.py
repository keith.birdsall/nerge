from django.contrib.postgres.fields import ArrayField
from django.core.validators import RegexValidator
from django.db import models
from django.db.models import JSONField
from django_prometheus.models import ExportModelOperationsMixin
from django.core.validators import MinValueValidator, MaxValueValidator


class BaseNetwork(ExportModelOperationsMixin("network"), models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=63)
    type = models.CharField(max_length=50, blank=True)
    description = models.CharField(max_length=200, blank=True)


class Vrf(ExportModelOperationsMixin("vrf"), models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(
        max_length=63,
        validators=[
            RegexValidator(
                regex=r"^Vrf-[A-Z]{3}-[A-Z]{4}$",
                message="name must be in all upper-case letters, and follow this schema:\n \
                        'Data Center'-'Environment'-'optional: Area'RDC,ESAE, etc'-'IP/INP/EP/ENP/LAB'-'optional: secure area'",
            )
        ],
    )
    network = models.CharField(max_length=63, blank=True)
    interface = models.CharField(max_length=100, blank=True)


class L3Vni(ExportModelOperationsMixin("l3vni"), models.Model):
    l3_vni = models.IntegerField(
        blank=True,
        null=True,
        validators=[MinValueValidator(1), MaxValueValidator(16777215)],
    )


class Tenant(ExportModelOperationsMixin("tenant"), models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(
        max_length=63,
        validators=[
            RegexValidator(
                regex=r"^[A-Z]{3}-[A-Z]{4}-(IP|INP|EP|ENP)$",
                message="name must be in all upper-case letters, and follow this schema:\n \
                        'Data Center'-'Environment'-'IP/INP/EP/ENP'",
            )
        ],
    )


class EndpointGroup(ExportModelOperationsMixin("epg"), models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(
        max_length=63,
        validators=[
            RegexValidator(
                regex=r"^[A-Z]{3}-[A-Z]{4}-EPG-[0-9]{0,4}$",
                message="end point group_name must be in all upper-case letters, and follow this schema:\n \
                        'Data Center'-'Environment'-'EPG'-'vlan tag'",
            )
        ],
    )
    ports = ArrayField(
        models.CharField(max_length=50, blank=True, null=True), blank=True, null=True
    )
    vlan = models.IntegerField(blank=True, null=True)
    vni = models.IntegerField(blank=True, null=True)


class BridgeDomain(ExportModelOperationsMixin("bridge_domain"), models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(
        max_length=63,
        validators=[
            RegexValidator(
                regex=r"^[A-Z]{3}-[A-Z]{4}-BD-[0-9]{0,4}$",
                message="name must be in all upper-case letters, and follow this schema:\n \
                        'Data Center'-'Environment'-'BD'-'vlan tag'",
            )
        ],
    )


class Interface(ExportModelOperationsMixin("interface"), models.Model):
    id = models.AutoField(primary_key=True)
    port = models.CharField(max_length=50)
    vlan = models.IntegerField(
        blank=True,
        null=True,
        validators=[MinValueValidator(1), MaxValueValidator(4094)],
    )
    ipv4 = models.CharField(
        max_length=50,
        blank=True,
    )
    network = models.CharField(max_length=63, blank=True)
    device = models.CharField(max_length=50, blank=True)
    description = models.CharField(max_length=200, blank=True)
    tagging_mode = models.CharField(max_length=50)


class Vlan(ExportModelOperationsMixin("vlan"), models.Model):
    id = models.AutoField(primary_key=True)
    vlan = models.IntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(4094)],
    )
    name = models.CharField(
        max_length=63,
    )
    network = models.CharField(max_length=63, blank=True)
    description = models.CharField(max_length=200, blank=True)

"""
class Snapshot(ExportModelOperationsMixin("snapshot"), models.Model):
    id = models.AutoField(primary_key=True)
    device = models.CharField(max_length=63, blank=True)
    network = models.CharField(max_length=63, blank=True)
    filename = models.CharField(max_length=200, blank=True)
    config = JSONField(default={"run": "run"})
    created_at = models.DateTimeField(auto_now_add=True)
"""
