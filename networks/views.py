import networks.models
import utils.helpers
from asgiref.sync import sync_to_async
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import generic


class Index(generic.View):
    async def get(self, request, name=None, network_type=None):
        if not await utils.helpers.is_authenticated(request):
            return HttpResponseRedirect("/login/")
        if name:
            object = await sync_to_async(
                networks.models.BaseNetwork.objects.get, thread_sensitive=True
            )(name=name)
            objects = [object]
        elif network_type:
            objects = await sync_to_async(list)(
                networks.models.BaseNetwork.objects.filter(type=network_type).order_by(
                    "name"
                ),
            )
        else:
            objects = await sync_to_async(list)(
                networks.models.BaseNetwork.objects.all().order_by("name")
            )
        network_objs = []
        for object in objects:
            object = object.__dict__
            network_objs.append(object)
        return render(
            request,
            "networks/index.html",
            {"networks": network_objs},
        )
