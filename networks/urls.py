from django.urls import path
import networks.views

app_name = "networks"

urlpatterns = [
    path("", networks.views.Index.as_view(), name="index"),
    path("name/<str:name>", networks.views.Index.as_view(), name="network"),
    path(
        "type/<str:network_type>", networks.views.Index.as_view(), name="networks_type"
    ),
]
