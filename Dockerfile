FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive\
  HTTP_PROXY=${HTTP_PROXY} \
  HTTPS_PROXY=${HTTPS_PROXY} \
  NO_PROXY=${NO_PROXY}

WORKDIR /nsp

COPY . .


RUN \
  apt-get update && \
  apt-get install -y python3 python3-pip python3.10-venv curl gcc libpq-dev libzmq3-dev salt-master nano systemctl iputils-ping dnsutils


RUN update-ca-certificates --fresh

RUN python3 -m venv /opt/venv
RUN /opt/venv/bin/pip install --upgrade pip
RUN /opt/venv/bin/pip install -r requirements.txt

CMD [ "./run.sh"]
