from django.urls import path
import settings.views

app_name = "settings"

urlpatterns = [
    path("", settings.views.index, name="index"),
]
