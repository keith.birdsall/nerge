import logging
import time
import datetime
from numbers import Number


class LogfmtFormatter(logging.Formatter):
    def format(self, record):
        record.asctime = self.format_time(record)
        fmt_msg = record.getMessage().replace('"', '\\"')
        extra_ctx = getattr(record, "context", {})
        log_format_msg = (
            f"time={record.asctime}",
            f"level={record.levelname}",
            f"location={record.filename}:${record.lineno}:{record.funcName}",
            f'msg="{fmt_msg}"',
            self.format_log_line(extra_ctx),
        )
        if record.exc_info:
            fmt_exc = self.formatException(record.exc_info).replace('"', '\\"')
            log_format_msg += (f'exception="{fmt_exc}"',)
        if record.stack_info:
            fmt_stack = self.formatStack(record.stack_info).replace('"', '\\"')
            log_format_msg += (f'stack="{fmt_stack}"',)
        return " ".join(log_format_msg)

    def format_time(self, record: logging.LogRecord):
        ct = self.converter()
        ct_with_ms = time.mktime(ct) + (record.msecs / 1000)
        ct_full = datetime.datetime.fromtimestamp(ct_with_ms)
        return ct_full.isoformat()

    def format_log_line(self, extra_ctx):
        outarr = []
        for k, val in extra_ctx.items():
            if val is None:
                outarr.append(f"{k}=")
                continue

            if isinstance(val, bool):
                val = "true" if val else "false"
            elif isinstance(val, Number):
                pass
            else:
                if isinstance(val, (dict, object)):
                    val = str(val)
                escaped_val = val.replace('"', '\\"')
                val = f'"{escaped_val}"'
            outarr.append(f"{k}={val}")
        return " ".join(outarr)
