"""nsp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import api.views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path

from drf_yasg import openapi
from drf_yasg.views import get_schema_view

from rest_framework import routers, permissions
from rest_framework.authtoken import views
from django.template.defaulttags import register


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


openapi_info = openapi.Info(
    title="NSP",
    default_version="0.1.0",
    description="Network Science Platform",
    contact=openapi.Contact(email="keith.birdsall@aicloud.guru"),
    license=openapi.License(name="MIT License"),
)

schema_view = get_schema_view(
    openapi_info,
    public=True,
    permission_classes=[permissions.AllowAny],
)


class OptionalSlashRouter(routers.DefaultRouter):
    def __init__(self):
        super().__init__()
        self.trailing_slash = "/?"


api_router = OptionalSlashRouter()

api_router.register(r"admin/users", api.views.UserViewSet)
api_router.register(r"admin/groups", api.views.GroupViewSet)

api_router.register(r"tenants", api.views.TenantViewSet)
api_router.register(r"epgs", api.views.EndpointGroupViewSet)
api_router.register(r"jobs", api.views.JobViewSet)
api_router.register(r"executions", api.views.JobExecutionViewSet)
api_router.register(r"results", api.views.JobResultViewSet)
api_router.register(r"devices", api.views.DeviceViewSet)
api_router.register(r"dashboards", api.views.DashboardViewSet)
api_router.register(r"networks", api.views.BaseNetworkViewSet)
api_router.register(r"vrfs", api.views.VrfViewSet)
api_router.register(r"l3vnis", api.views.L3VniViewSet)
api_router.register(r"vlans", api.views.VlanViewSet)
api_router.register(r"interfaces", api.views.InterfaceViewSet)
#api_router.register(r"snapshots", api.views.SnapshotViewSet)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    path("api-token-auth/", views.obtain_auth_token),
    path("api/", include(api_router.urls)),
    # path("api/epgs/", api.views.EndpointGroupList.as_view()),
    # path("api/epgs/<int:id>/", api.views.EndpointGroupDetail.as_view()),
    path("login/", include("login.urls")),
    path("networks/", include("networks.urls")),
    path("jobs/", include("jobs.urls")),
    path("dashboards/", include("dashboards.urls")),
    path("settings/", include("settings.urls")),
    path("devices/", include("devices.urls")),
    re_path(
        r"^api/swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    re_path(
        r"^api/swagger/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    re_path(
        r"^api/redoc/$",
        schema_view.with_ui("redoc", cache_timeout=0),
        name="schema-redoc",
    ),
    path("", include("devices.urls")),
    path("", include("django_prometheus.urls")),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
