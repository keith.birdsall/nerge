"""
ASGI config for nsp project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.1/howto/deployment/asgi/
"""

import os
import nsp.consumers

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.core.asgi import get_asgi_application
from django.urls import path, re_path

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nsp.settings")
django_asgi_app = get_asgi_application()

application = ProtocolTypeRouter(
    {
        "http": django_asgi_app,
        "websocket": AuthMiddlewareStack(
            URLRouter(
                [
                    re_path(
                        r"ws/dashboards/(?P<uuid>.*)/$",
                        nsp.consumers.DashboardsConsumer.as_asgi(),
                    ),
                    # path(
                    #     "ws/dashboards/",
                    #     nsp.consumers.DashboardsConsumer.as_asgi(),
                    # ),
                    re_path(
                        r"ws/devices/(?P<uuid>.*)/$",
                        nsp.consumers.DevicesConsumer.as_asgi(),
                    ),
                    re_path(
                        r"ws/networks/(?P<uuid>.*)/$",
                        nsp.consumers.NetworksConsumer.as_asgi(),
                    ),
                    re_path(
                        r"ws/executions/(?P<uuid>.*)/$",
                        nsp.consumers.ExecutionsConsumer.as_asgi(),
                    ),
                    re_path(
                        r"ws/jobs/(?P<uuid>.*)/$",
                        nsp.consumers.JobsConsumer.as_asgi(),
                    ),
                    re_path(
                        r"ws/results/(?P<uuid>.*)/$",
                        nsp.consumers.ResultsConsumer.as_asgi(),
                    ),
                ]
            )
        ),
    }
)
