import json
import utils.helpers
import logging
import pickle
from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from datetime import datetime

logger = logging.getLogger(__name__)


class BaseModelsConsumer(AsyncWebsocketConsumer):
    async def _get_dashboards(self, **kwargs):
        from dashboards.models import BaseDashboard

        results = []
        try:
            results = await database_sync_to_async(list)(
                BaseDashboard.objects.filter(**kwargs).order_by("name")
            )
        except Exception as e:
            logger.error(e)
        return results

    async def _get_devices(self, **kwargs):
        from devices.models import BaseNetworkDevice

        results = []
        try:
            results = await database_sync_to_async(list)(
                BaseNetworkDevice.objects.filter(**kwargs).order_by("name")
            )
        except Exception as e:
            logger.error(e)
        return results

    async def _get_networks(self, **kwargs):
        from networks.models import BaseNetwork

        results = []
        try:
            results = await database_sync_to_async(list)(
                BaseNetwork.objects.filter(**kwargs).order_by("name")
            )
        except Exception as e:
            logger.error(e)
        return results

    async def _get_results(self, **kwargs):
        from jobs.models import BaseJobResult

        results = []
        try:
            results = await database_sync_to_async(list)(
                BaseJobResult.objects.filter(**kwargs).order_by("-job_execution_id")[
                    :1000
                ]
            )
        except Exception as e:
            logger.error(e)
        return results

    async def _get_jobs(self, **kwargs):
        from django_apscheduler.models import DjangoJob

        results = []
        try:
            results = await database_sync_to_async(list)(
                DjangoJob.objects.filter(**kwargs)[:1000]
            )
        except Exception as e:
            logger.error(e)
        return results

    async def _get_executions(self, **kwargs):
        from django_apscheduler.models import DjangoJobExecution

        results = []
        try:
            results = await database_sync_to_async(list)(
                DjangoJobExecution.objects.filter(**kwargs)[:1000]
            )
        except Exception as e:
            logger.error(e)
        return results

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(self.group_name, self.channel_name)

    async def receive(self, text_data):
        self.text_data_json = json.loads(text_data)
        message = self.text_data_json.get("message")
        self.django_model_search = self.text_data_json.get("django_model_search")
        self.subscribe_to_updates = self.text_data_json.get("subscribe_to_updates")
        if message:
            await self.channel_layer.group_send(
                self.group_name, {"type": "event.message", "message": message}
            )
        if self.django_model_search:
            self.filter = self.text_data_json.get("django_model_filter") or {}
            await self._send(**self.filter)

    async def event_message(self, event):
        event_message = {
            "message": event["message"],
            **(
                {"subscribe_to_updates": True}
                if self.__dict__.get("subscribe_to_updates")
                else {}
            ),
        }
        if issubclass(type(event_message["message"]), list) and self.__dict__.get(
            "django_model_search"
        ):
            if not self.old_val:
                self.old_val = event_message["message"]
            if self.old_val and self.old_val != event_message["message"]:
                event_message["old_val"] = self.old_val
                self.old_val = event_message["message"]
        await self.send(text_data=json.dumps(event_message))


class DevicesConsumer(BaseModelsConsumer):
    async def _send(self, **kwargs):
        devices = []
        for device in await self._get_devices(**kwargs):
            device = device.__dict__
            data_map = {
                "_state": None,
                "id": None,
            }
            device = utils.helpers.set_dict_vals(device, data_map)
            devices.append(device)
        self.new_val = devices
        devices_message = {"type": "event.message", "message": self.new_val}
        await self.channel_layer.group_send(self.group_name, devices_message)

    async def connect(self):
        self.group_name = f"devices-{self.scope['url_route']['kwargs']['uuid']}"
        self.old_val = None
        await self.channel_layer.group_add(self.group_name, self.channel_name)
        await self.accept()
        await self.channel_layer.group_send(
            self.group_name, {"type": "event.message", "message": "connected"}
        )


class ExecutionsConsumer(BaseModelsConsumer):
    async def _send(self, **kwargs):
        executions = []
        for execution in await self._get_executions(**kwargs):
            execution = execution.__dict__
            finished = execution["finished"]
            duration = execution["duration"]
            if finished:
                finished = datetime.fromtimestamp(
                    float(execution["finished"])
                ).isoformat()
            if duration:
                duration = str(duration)
            data_map = {
                "_state": None,
                "run_time": execution["run_time"].isoformat(),
                "finished": finished or "-",
                "duration": duration or "-",
            }
            execution = utils.helpers.set_dict_vals(execution, data_map)
            executions.append(execution)
        self.new_val = executions
        executions_message = {"type": "event.message", "message": self.new_val}
        await self.channel_layer.group_send(self.group_name, executions_message)

    async def connect(self):
        self.group_name = f"executions-{self.scope['url_route']['kwargs']['uuid']}"
        self.old_val = None
        await self.channel_layer.group_add(self.group_name, self.channel_name)
        await self.accept()
        await self.channel_layer.group_send(
            self.group_name, {"type": "event.message", "message": "connected"}
        )


class ResultsConsumer(BaseModelsConsumer):
    async def _send(self, **kwargs):
        results = []
        for result in await self._get_results(**kwargs):
            result = result.__dict__
            stdout = result["stdout"] or " "
            data_map = {
                "_state": None,
                "id": None,
                "stdout": stdout.replace("\n", " "),
            }
            result = utils.helpers.set_dict_vals(result, data_map)
            results.append(result)
        self.new_val = results
        results_message = {"type": "event.message", "message": self.new_val}
        await self.channel_layer.group_send(self.group_name, results_message)

    async def connect(self):
        self.group_name = f"results-{self.scope['url_route']['kwargs']['uuid']}"
        self.old_val = None
        await self.channel_layer.group_add(self.group_name, self.channel_name)
        await self.accept()
        await self.channel_layer.group_send(
            self.group_name, {"type": "event.message", "message": "connected"}
        )


class DashboardsConsumer(BaseModelsConsumer):
    async def _send(self, **kwargs):
        dashboards = []
        try:
            for dashboard in await self._get_dashboards(**kwargs):
                dashboard = dashboard.__dict__
                data_map = {
                    "_state": None,
                }
                dashboard = utils.helpers.set_dict_vals(dashboard, data_map)
                dashboards.append(dashboard)
            self.new_val = dashboards
            dashboards_message = {"type": "event.message", "message": self.new_val}
        except Exception as e:
            logger.error(e)
        await self.channel_layer.group_send(self.group_name, dashboards_message)

    async def connect(self):
        uuid = self.scope["url_route"]["kwargs"].get("uuid")
        self.group_name = f"dashboards-{uuid}" if uuid else "dashboards"
        self.old_val = None
        await self.channel_layer.group_add(self.group_name, self.channel_name)
        await self.accept()
        await self.channel_layer.group_send(
            self.group_name, {"type": "event.message", "message": "connected"}
        )


class JobsConsumer(BaseModelsConsumer):
    async def _send(self, **kwargs):
        jobs = []
        for job in await self._get_jobs(**kwargs):
            job = job.__dict__
            job_state = pickle.loads(job["job_state"])
            next_run_time = job.get("next_run_time")
            if next_run_time:
                next_run_time = next_run_time.isoformat()
            job = utils.helpers.set_dict_vals(
                job,
                {
                    "_state": None,
                    "job_state": None,
                    "func": job_state["func"],
                    "args": job_state["args"],
                    "kwargs": job_state["kwargs"],
                    "max_instances": job_state["max_instances"],
                },
            )
            job["next_run_time"] = next_run_time
            jobs.append(job)
        self.new_val = jobs
        jobs_message = {"type": "event.message", "message": self.new_val}
        await self.channel_layer.group_send(self.group_name, jobs_message)

    async def connect(self):
        self.group_name = f"jobs-{self.scope['url_route']['kwargs']['uuid']}"
        self.old_val = None
        await self.channel_layer.group_add(self.group_name, self.channel_name)
        await self.accept()
        await self.channel_layer.group_send(
            self.group_name, {"type": "event.message", "message": "connected"}
        )


class NetworksConsumer(BaseModelsConsumer):
    async def _send(self, **kwargs):
        networks = []
        for network in await self._get_networks(**kwargs):
            network = network.__dict__
            description = network.get("description")
            data_map = {"_state": None, "description": description or " ", "id": None}
            network = utils.helpers.set_dict_vals(network, data_map)
            networks.append(network)
        self.new_val = networks
        networks_message = {"type": "event.message", "message": self.new_val}
        await self.channel_layer.group_send(self.group_name, networks_message)

    async def connect(self):
        self.group_name = f"networks-{self.scope['url_route']['kwargs']['uuid']}"
        self.old_val = None
        await self.channel_layer.group_add(self.group_name, self.channel_name)
        await self.accept()
        await self.channel_layer.group_send(
            self.group_name, {"type": "event.message", "message": "connected"}
        )
