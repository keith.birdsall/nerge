from django.shortcuts import render
from django.views import generic
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt


class Index(generic.View):
    async def get(self, request, **kwargs):
        return render(
            request,
            "login/index.html",
        )
