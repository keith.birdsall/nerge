from django.urls import path
import login.views

app_name = "login"

urlpatterns = [
    path("", login.views.Index.as_view(), name="index"),
    path("login/", login.views.login, name="login"),
]
