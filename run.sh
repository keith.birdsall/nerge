#!/bin/bash
cat <<EOF > /etc/resolv.conf
nameserver 192.168.4.44
EOF

if [ "$ENVIRONMENT" == "development" ];
then
  /usr/bin/python3 /usr/bin/salt-master
else
  if ! SCHEDULER_AUTOSTART=False /opt/venv/bin/python manage.py migrate --check
  then
    SCHEDULER_AUTOSTART=False /opt/venv/bin/python manage.py migrate
  fi
  service salt-master start
  /opt/venv/bin/python manage.py collectstatic --noinput
  /opt/venv/bin/python -m gunicorn --bind 0.0.0.0:80 --preload nsp.asgi:application -k uvicorn.workers.UvicornWorker -w $UVICORN_WORKERS
fi
