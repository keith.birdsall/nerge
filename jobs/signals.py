from django_apscheduler.models import DjangoJob, DjangoJobExecution
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
import jobs.models
import utils.helpers
import logging
import pickle
import json

logger = logging.getLogger(__name__)


@receiver(
    post_save,
    sender=DjangoJob,
    dispatch_uid="job_save",
)
def job_save(sender, instance, **kwargs):
    job_state = pickle.loads(instance.__dict__["job_state"])
    next_run_time = job_state.get("next_run_time")
    if next_run_time:
        next_run_time = next_run_time.isoformat()
    instance_dict = utils.helpers.set_dict_vals(
        instance.__dict__,
        {
            "_state": None,
            "job_state": None,
            "func": str(job_state["func"]),
            "args": job_state["args"],
            "kwargs": job_state["kwargs"],
            "max_instances": job_state["max_instances"],
            "next_run_time": next_run_time,
        },
    )
    log_ctx = {"context": instance_dict}
    logger.info("saving job", extra=log_ctx)


@receiver(
    pre_delete,
    sender=DjangoJob,
    dispatch_uid="job_delete",
)
def job_delete(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("deleting job", extra=log_ctx)


@receiver(
    post_save,
    sender=DjangoJobExecution,
    dispatch_uid="execution_save",
)
def execution_save(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("saving execution", extra=log_ctx)


@receiver(
    pre_delete,
    sender=DjangoJobExecution,
    dispatch_uid="execution_delete",
)
def execution_delete(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("deleting job", extra=log_ctx)


@receiver(
    post_save,
    sender=jobs.models.BaseJobResult,
    dispatch_uid="result_save",
)
def result_save(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(
        instance.__dict__, {"_state": None, "stdout": None}
    )
    log_ctx = {"context": instance_dict}
    logger.info("saving execution", extra=log_ctx)


@receiver(
    pre_delete,
    sender=jobs.models.BaseJobResult,
    dispatch_uid="result_delete",
)
def result_delete(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("deleting job", extra=log_ctx)
