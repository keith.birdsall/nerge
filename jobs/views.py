import jobs.models
import utils.helpers
import pickle
from asgiref.sync import sync_to_async
from django_apscheduler.models import DjangoJobExecution, DjangoJob
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import generic
from datetime import datetime


class Index(generic.View):
    async def get(self, request):
        if not await utils.helpers.is_authenticated(request):
            return HttpResponseRedirect("/login/")
        objects = await sync_to_async(list)(DjangoJob.objects.all())
        jobs = []
        for object in objects:
            object = object.__dict__
            job_state = pickle.loads(object["job_state"])
            next_run_time = object.get("next_run_time")
            if next_run_time:
                next_run_time = next_run_time.isoformat()
            job = utils.helpers.set_dict_vals(
                object,
                {
                    "_state": None,
                    "job_state": None,
                    "func": job_state["func"],
                    "args": f'{job_state["args"]}',
                    "kwargs": job_state["kwargs"],
                    "max_instances": job_state["max_instances"],
                    "next_run_time": next_run_time,
                },
            )
            jobs.append(job)
        return render(
            request,
            "jobs/index.html",
            {"jobs": jobs},
        )


class Executions(generic.View):
    async def get(self, request, id=None, job_id=None):
        if not await utils.helpers.is_authenticated(request):
            return HttpResponseRedirect("/login/")
        if id:
            object = await sync_to_async(
                DjangoJobExecution.objects.get, thread_sensitive=True
            )(id=id)
            objects = [object]
        elif job_id:
            objects = await sync_to_async(list)(
                DjangoJobExecution.objects.filter(job_id=job_id)[:1000]
            )
        else:
            objects = await sync_to_async(list)(DjangoJobExecution.objects.all()[:1000])
        executions = []
        for object in objects:
            object = object.__dict__
            finished = object["finished"]
            duration = object["duration"]
            if finished:
                finished = datetime.fromtimestamp(float(object["finished"])).isoformat()
            data_map = {
                "_state": None,
                "run_time": object["run_time"].isoformat(),
                "finished": finished or "-",
                "duration": duration or "-",
                "exception": object["exception"] or "",
                "traceback": object["traceback"] or "",
            }
            executions.append(utils.helpers.set_dict_vals(object, data_map))
        return render(
            request,
            "jobs/executions.html",
            {"executions": executions},
        )


class Results(generic.View):
    async def get(self, request, job_execution_id=None, job=None):
        if not await utils.helpers.is_authenticated(request):
            return HttpResponseRedirect("/login/")
        if job_execution_id:
            results = await sync_to_async(list)(
                jobs.models.BaseJobResult.objects.filter(
                    job_execution_id=job_execution_id
                ).order_by("-job_execution_id")
            )
        elif job:
            results = await sync_to_async(list)(
                jobs.models.BaseJobResult.objects.filter(job_id=job).order_by(
                    "-job_execution_id"
                )[:1000]
            )
        else:
            results = await sync_to_async(list)(
                jobs.models.BaseJobResult.objects.all().order_by("-job_execution_id")[
                    :1000
                ]
            )
        return render(
            request,
            "jobs/results.html",
            {"results": results},
        )
