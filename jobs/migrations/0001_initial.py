# Generated by Django 4.1.11 on 2023-10-12 13:57

from django.db import migrations, models
import django_prometheus.models


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="BaseJobResult",
            fields=[
                ("id", models.AutoField(primary_key=True, serialize=False)),
                ("job_execution_id", models.IntegerField(blank=True)),
                ("job_id", models.CharField(blank=True, max_length=100, null=True)),
                ("cmd", models.CharField(blank=True, max_length=255, null=True)),
                ("stdout", models.TextField()),
            ],
            bases=(
                django_prometheus.models.ExportModelOperationsMixin("result"),
                models.Model,
            ),
        ),
    ]
