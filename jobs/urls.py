from django.urls import path
import jobs.views
import jobs.models

app_name = "jobs"

urlpatterns = [
    path("", jobs.views.Index.as_view(), name="index"),
    path("executions/", jobs.views.Executions.as_view(), name="executions"),
    path("executions/id/<int:id>", jobs.views.Executions.as_view(), name="execution"),
    path(
        "executions/job/<str:job_id>",
        jobs.views.Executions.as_view(),
        name="executions_job",
    ),
    path("results/", jobs.views.Results.as_view(), name="results"),
    path(
        "results/id/<int:job_execution_id>", jobs.views.Results.as_view(), name="result"
    ),
    path("results/job/<str:job>", jobs.views.Results.as_view(), name="results_job"),
]
