import clients.salt
import jobs.models
import logging
from apscheduler.schedulers.background import BackgroundScheduler
from datetime import datetime
from django_apscheduler.jobstores import register_events
from django_apscheduler.models import DjangoJobExecution
from django.conf import settings
from django_apscheduler import util
#from networks.models import Snapshot

import json
from io import BytesIO
from django.core import serializers

logger = logging.getLogger(__name__)
background_scheduler = BackgroundScheduler(settings.SCHEDULER_CONFIG)


@util.close_old_connections
def delete_old_job_executions(max_age=604_800):
    """
    This job deletes APScheduler job execution entries older than `max_age` from the database.
    It helps to prevent the database from filling up with old historical records that are no
    longer useful.

    :param max_age: The maximum length of time to retain historical job execution records.
                    Defaults to 7 days.
    """
    DjangoJobExecution.objects.delete_old_job_executions(max_age)


@util.close_old_connections
def discover_network_devices():
    client = clients.salt.Client()
    salt_keys = client.list_keys()
    salt_keys_cmd = client.cmd
    salt_keys_stdout = salt_keys.get("_stdout")
    unaccepted_keys = salt_keys.get("Unaccepted Keys")
    execution = DjangoJobExecution.objects.filter(
        job_id="discover_network_devices"
    ).latest("id")
    job_execution_id = execution.id
    job_id = execution.job_id
    salt_keys_results = jobs.models.BaseJobResult(
        job_execution_id=job_execution_id,
        job_id=job_id,
        cmd=salt_keys_cmd,
        stdout=salt_keys_stdout,
    )
    salt_keys_results.save()
    process_device_results = []
    if unaccepted_keys:
        for key in unaccepted_keys:
            process_device_result = client.process_device(key)
            process_device_cmd = client.cmd
            if process_device_result:
                process_device_results.append(
                    {process_device_cmd: process_device_result.stdout.decode("utf-8")}
                )
    if process_device_results:
        for result in process_device_results:
            cmd, stdout = list(result.items())[0]
            process_keys_results = jobs.models.BaseJobResult(
                job_execution_id=job_execution_id,
                job_id=job_id,
                cmd=cmd,
                stdout=stdout,
            )
            process_keys_results.save()


# @util.close_old_connections
def sonic_merge_config(*args, **config):
    device_name = args[0]
    signal = args[1]
    client = clients.salt.Client()
    cp_merge_candidate = client.cp_merge_candidate(device_name, **config)
    cp_merge_candidate_path = cp_merge_candidate.get("tmp_file_path")
    cp_merge_candidate_stdout = cp_merge_candidate.get("stdout")
    cp_merge_candidate_cmd = client.cmd
    logger.info(
        msg="copied merge candidate to device",
        extra={
            "context": {
                "signal": signal,
                "device": device_name,
                "tmp_file_path": cp_merge_candidate_path,
                "cmd": cp_merge_candidate_cmd,
            }
        },
    )
    execution = DjangoJobExecution.objects.filter(
        job_id=f"sonic_merge_config_{signal}"
    ).latest("id")
    job_execution_id = execution.id
    job_id = execution.job_id
    cp_merge_candidate_results = jobs.models.BaseJobResult(
        job_execution_id=job_execution_id,
        job_id=job_id,
        cmd=cp_merge_candidate_cmd,
        stdout=cp_merge_candidate_stdout.decode("utf-8"),
    )
    cp_merge_candidate_results.save()
    merge_config = client.merge_config(device_name, cp_merge_candidate_path)
    merge_config_cmd = client.cmd
    logger.info(
        msg="merged config candidate to running configuration",
        extra={
            "context": {
                "signal": signal,
                "device": device_name,
                "cmd": merge_config_cmd,
            }
        },
    )
    merge_config_results = jobs.models.BaseJobResult(
        job_execution_id=job_execution_id,
        job_id=job_id,
        cmd=merge_config_cmd,
        stdout=merge_config.stdout.decode("utf-8"),
    )
    merge_config_results.save()
    save_config = client.save_config(device_name)
    save_config_cmd = client.cmd
    save_config_results = jobs.models.BaseJobResult(
        job_execution_id=job_execution_id,
        job_id=job_id,
        cmd=save_config_cmd,
        stdout=save_config.stdout.decode("utf-8"),
    )
    save_config_results.save()
    logger.info(
        msg="saved running configuration",
        extra={
            "context": {"signal": signal, "device": device_name, "cmd": save_config_cmd}
        },
    )
    job = background_scheduler.get_job(job_id)
    job.pause()


# @util.close_old_connections
def sonic_exec_config(*args, **kwargs):
    device_name = args[0]
    signal = args[1]
    cmd = kwargs.get("cmd")
    client = clients.salt.Client()
    exec_config = client.exec_config(device_name, cmd)
    exec_config_cmd = client.cmd
    execution = DjangoJobExecution.objects.filter(
        job_id=f"sonic_exec_config_{signal}"
    ).latest("id")
    job_execution_id = execution.id
    job_id = execution.job_id
    exec_config_results = jobs.models.BaseJobResult(
        job_execution_id=job_execution_id,
        job_id=job_id,
        cmd=exec_config_cmd,
        stdout=exec_config.stdout.decode("utf-8"),
    )
    exec_config_results.save()
    save_config = client.save_config(device_name)
    save_config_cmd = client.cmd
    save_config_results = jobs.models.BaseJobResult(
        job_execution_id=job_execution_id,
        job_id=job_id,
        cmd=save_config_cmd,
        stdout=save_config.stdout.decode("utf-8"),
    )
    save_config_results.save()
    logger.info(
        msg="saved running configuration",
        extra={
            "context": {"signal": signal, "device": device_name, "cmd": save_config_cmd}
        },
    )
    job = background_scheduler.get_job(job_id)
    job.pause()


@util.close_old_connections
def sonic_fabric_add_leaf(*args, **kwargs):
    device_name = args[0]
    signal = args[1]
    cmd = kwargs.get("cmd")
    client = clients.salt.Client()
    exec_config = client.exec_config(device_name, cmd)
    exec_config_cmd = client.cmd
    execution = DjangoJobExecution.objects.filter(
        job_id="sonic_fabric_add_leaf"
    ).latest("id")
    job_execution_id = execution.id
    job_id = execution.job_id
    exec_config_results = jobs.models.BaseJobResult(
        job_execution_id=job_execution_id,
        job_id=job_id,
        cmd=exec_config_cmd,
        stdout=exec_config.stdout.decode("utf-8"),
    )
    exec_config_results.save()
    save_config = client.save_config(device_name)
    save_config_cmd = client.cmd
    save_config_results = jobs.models.BaseJobResult(
        job_execution_id=job_execution_id,
        job_id=job_id,
        cmd=save_config_cmd,
        stdout=save_config.stdout.decode("utf-8"),
    )
    save_config_results.save()
    logger.info(
        msg="adding leaf to fabric network",
        extra={
            "context": {"signal": signal, "device": device_name, "cmd": save_config_cmd}
        },
    )
    job = background_scheduler.get_job(job_id)
    job.pause()


@util.close_old_connections
def sonic_fabric_add_spine(*args, **kwargs):
    device_name = args[0]
    signal = args[1]
    cmd = kwargs.get("cmd")
    client = clients.salt.Client()
    exec_config = client.exec_config(device_name, cmd)
    exec_config_cmd = client.cmd
    execution = DjangoJobExecution.objects.filter(
        job_id="sonic_fabric_add_spine"
    ).latest("id")
    job_execution_id = execution.id
    job_id = execution.job_id
    exec_config_results = jobs.models.BaseJobResult(
        job_execution_id=job_execution_id,
        job_id=job_id,
        cmd=exec_config_cmd,
        stdout=exec_config.stdout.decode("utf-8"),
    )
    exec_config_results.save()
    save_config = client.save_config(device_name)
    save_config_cmd = client.cmd
    save_config_results = jobs.models.BaseJobResult(
        job_execution_id=job_execution_id,
        job_id=job_id,
        cmd=save_config_cmd,
        stdout=save_config.stdout.decode("utf-8"),
    )
    save_config_results.save()
    logger.info(
        msg="adding spine to fabric network",
        extra={
            "context": {"signal": signal, "device": device_name, "cmd": save_config_cmd}
        },
    )
    job = background_scheduler.get_job(job_id)
    job.pause()

"""
def snap_config(*args, **kwargs):
    device_name = args[0]
    snap_id = args[1]
    cmd = kwargs.get("cmd")
    client = clients.salt.Client()
    exec_config = client.exec_config(device_name, cmd)
    json_string = exec_config.decode('utf-8')
    start_index = json_string.find('{')
    json_data = json_string[start_index:]
    json_data = json.loads(json_data)
    json_data = json.dumps(json_data, indent=4)
    execution = DjangoJobExecution.objects.filter(
        job_id="snapshot_save_config"
    ).latest("id")
    job_execution_id = execution.id
    job_id = execution.job_id
    exec_config_results = jobs.models.BaseJobResult(
        job_execution_id=job_execution_id,
        job_id=job_id,
        cmd=cmd,
        stdout="text",
    )
    exec_config_results.save()
    snap_id.config = {"config": json_data}
    snap_id.save()
    logger.info(
        msg="snapshot config saved",
        extra={
            "context": {"Device": device_name}
        },
    )
    job = background_scheduler.get_job(job_id)
    job.pause()
    print("THIS IS THE END !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
"""
def start():
    background_scheduler.add_job(
        func=discover_network_devices,
        trigger="cron",
        id="discover_network_devices",
        max_instances=10,
        replace_existing=True,
        second="*/50",
    )
    logger.info("Added job 'discover_network_devices'.")
    background_scheduler.add_job(
        func=delete_old_job_executions,
        trigger="cron",
        id="delete_old_job_executions",
        max_instances=1,
        replace_existing=True,
        day_of_week="mon",
        hour="00",
        minute="00",
    )
    """
    snapshot_save_config = background_scheduler.add_job(
        func=snap_config,
        args=(),
        kwargs={},
        trigger="interval",
        weeks=432,
        id="snapshot_save_config",
        max_instances=settings.JOBS_WORKERS,
        replace_existing=True,
        misfire_grace_time=None,
    )
    """
    sonic_merge_config_vrf_save = background_scheduler.add_job(
        func=sonic_merge_config,
        args=(),
        kwargs={},
        trigger="interval",
        weeks=432,
        id="sonic_merge_config_vrf_save",
        max_instances=settings.JOBS_WORKERS,
        replace_existing=True,
        misfire_grace_time=None,
    )
    sonic_merge_config_vlan_save = background_scheduler.add_job(
        func=sonic_merge_config,
        args=(),
        kwargs={},
        trigger="interval",
        weeks=432,
        id="sonic_merge_config_vlan_save",
        max_instances=settings.JOBS_WORKERS,
        replace_existing=True,
        misfire_grace_time=None,
    )
    sonic_merge_config_interface_save = background_scheduler.add_job(
        func=sonic_merge_config,
        args=(),
        kwargs={},
        trigger="interval",
        weeks=432,
        id="sonic_merge_config_interface_save",
        max_instances=settings.JOBS_WORKERS,
        replace_existing=True,
        misfire_grace_time=None,
    )
    sonic_exec_config_vrf_delete = background_scheduler.add_job(
        func=sonic_exec_config,
        args=(),
        kwargs={},
        trigger="interval",
        weeks=432,
        id="sonic_exec_config_vrf_delete",
        max_instances=settings.JOBS_WORKERS,
        replace_existing=True,
        misfire_grace_time=None,
    )
    sonic_exec_config_vlan_delete = background_scheduler.add_job(
        func=sonic_exec_config,
        args=(),
        kwargs={},
        trigger="interval",
        weeks=432,
        id="sonic_exec_config_vlan_delete",
        max_instances=settings.JOBS_WORKERS,
        replace_existing=True,
        misfire_grace_time=None,
    )
    sonic_exec_config_interface_delete = background_scheduler.add_job(
        func=sonic_exec_config,
        args=(),
        kwargs={},
        trigger="interval",
        weeks=432,
        id="sonic_exec_config_interface_delete",
        max_instances=settings.JOBS_WORKERS,
        replace_existing=True,
        misfire_grace_time=None,
    )
    sonic_merge_config_vrf_save.pause()
    sonic_merge_config_vlan_save.pause()
    sonic_merge_config_interface_save.pause()
    sonic_exec_config_vrf_delete.pause()
    sonic_exec_config_vlan_delete.pause()
    sonic_exec_config_interface_delete.pause()
    #snapshot_save_config.pause()
    logger.info("Added weekly job: 'delete_old_job_executions'.")
    try:
        logger.info("Starting background_scheduler...")
        register_events(background_scheduler)
        background_scheduler.start()
    except KeyboardInterrupt:
        logger.info("Stopping background_scheduler...")
        background_scheduler.shutdown()
        logger.info("Scheduler shut down successfully!")
