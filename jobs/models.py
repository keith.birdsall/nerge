from django.db import models
from django.contrib.postgres.fields import ArrayField
from django_prometheus.models import ExportModelOperationsMixin


class BaseJobResult(ExportModelOperationsMixin("result"), models.Model):
    id = models.AutoField(primary_key=True)
    job_execution_id = models.IntegerField(blank=True)
    job_id = models.CharField(max_length=100, blank=True, null=True)
    cmd = models.CharField(max_length=255, blank=True, null=True)
    stdout = models.TextField()
