setActiveBreadcrumbs("executions");
connectWebSocket("executions", uuidv4());
const uri = window.location.toString().split("executions/")[1];
let initialSearchBarValue = null;
if (uri.includes("id/")) {
  initialSearchBarValue = { id: uri.split("id/")[1] };
} else if (uri.includes("job/")) {
  initialSearchBarValue = {
    job_id: uri.split("job/")[1].replaceAll("%20", " "),
  };
} else {
  initialSearchBarValue = "";
}
initialSearchBarValue = Object.keys(initialSearchBarValue)
  .map((key) => `${key}:${initialSearchBarValue[key]}`)
  .join(" ");
searchBar.value = initialSearchBarValue;
