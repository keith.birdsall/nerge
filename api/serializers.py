import devices.models
import jobs.models
import networks.models
import dashboards.models
from django_apscheduler.models import DjangoJob, DjangoJobExecution
from django.contrib.auth.models import User, Group
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ["url", "username", "email", "groups", "id"]


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ["url", "name", "id"]


class BaseNetworkSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = networks.models.BaseNetwork
        fields = ["id", "name", "type", "description"]


class VrfSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = networks.models.Vrf
        fields = ["id", "name", "network", "interface"]


class L3VniSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = networks.models.L3Vni
        fields = ["l3_vni"]


class BaseNetworkDeviceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = devices.models.BaseNetworkDevice
        fields = ["id", "name", "network", "salt_key_status", "serial"]


class TenantSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = networks.models.Tenant
        fields = ["name", "id"]


class EndpointGroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = networks.models.EndpointGroup
        fields = ["name", "ports", "vlan", "vni", "id"]


class BridgeDomainSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = networks.models.BridgeDomain
        fields = ["name", "id"]


class JobSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DjangoJob
        fields = ["id", "next_run_time"]


class JobExecutionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DjangoJobExecution
        fields = [
            "id",
            "job_id",
            "status",
            "run_time",
            "duration",
            "finished",
            "exception",
            "traceback",
        ]


class JobResultSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = jobs.models.BaseJobResult
        fields = ["id", "job_execution_id", "cmd", "stdout"]


class DashboardSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = dashboards.models.BaseDashboard
        fields = ["name", "id"]


class VlanSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = networks.models.Vlan
        fields = ["id", "vlan", "name", "network", "description"]


class InterfaceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = networks.models.Interface
        fields = [
            "id",
            "port",
            "vlan",
            "ipv4",
            "network",
            "device",
            "description",
            "tagging_mode",
        ]
"""
class SnapshotSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = networks.models.Snapshot
        fields = ["id", "device", "network", "filename"]
"""
