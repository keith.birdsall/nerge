import devices.models
import jobs.models
import networks.models
import dashboards.models
from django_apscheduler.models import DjangoJob, DjangoJobExecution
from django.contrib.auth.models import User, Group
from django.http import Http404
from rest_framework import viewsets, status, permissions, response
from rest_framework.views import APIView
from drf_yasg.utils import swagger_auto_schema
from api.serializers import (
    JobSerializer,
    JobExecutionSerializer,
    JobResultSerializer,
    UserSerializer,
    GroupSerializer,
    EndpointGroupSerializer,
    TenantSerializer,
    BaseNetworkSerializer,
    BaseNetworkDeviceSerializer,
    DashboardSerializer,
    VrfSerializer,
    L3VniSerializer,
    InterfaceSerializer,
    VlanSerializer,
    #SnapshotSerializer,
)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = User.objects.all().order_by("-date_joined")
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """

    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class BaseNetworkViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows networks to be viewed or edited.
    """

    queryset = networks.models.BaseNetwork.objects.all().order_by()
    serializer_class = BaseNetworkSerializer
    permission_classes = [permissions.IsAuthenticated]


class VrfViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows vrfs to be viewed or edited.
    """

    queryset = networks.models.Vrf.objects.all().order_by()
    serializer_class = VrfSerializer
    permission_classes = [permissions.IsAuthenticated]


class L3VniViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows l3vnis to be viewed or edited.
    """

    queryset = networks.models.L3Vni.objects.all().order_by()
    serializer_class = L3VniSerializer
    permission_classes = [permissions.IsAuthenticated]


class DeviceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows devices to be viewed or edited.
    """

    queryset = devices.models.BaseNetworkDevice.objects.all().order_by()
    serializer_class = BaseNetworkDeviceSerializer
    permission_classes = [permissions.IsAuthenticated]


class TenantViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows tenants to be viewed or edited.
    """

    queryset = networks.models.Tenant.objects.all().order_by()
    serializer_class = TenantSerializer
    permission_classes = [permissions.IsAuthenticated]


class EndpointGroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows egps to be viewed or edited.
    """

    queryset = networks.models.EndpointGroup.objects.all()
    serializer_class = EndpointGroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class JobViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows egps to be viewed or edited.
    """

    queryset = DjangoJob.objects.all()
    serializer_class = JobSerializer
    permission_classes = [permissions.IsAuthenticated]


class JobExecutionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows egps to be viewed or edited.
    """

    queryset = DjangoJobExecution.objects.all()
    serializer_class = JobExecutionSerializer
    permission_classes = [permissions.IsAuthenticated]


class JobResultViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows egps to be viewed or edited.
    """

    queryset = jobs.models.BaseJobResult.objects.all().order_by("-job_execution_id")
    serializer_class = JobResultSerializer
    permission_classes = [permissions.IsAuthenticated]


class DashboardViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows egps to be viewed or edited.
    """

    queryset = dashboards.models.BaseDashboard.objects.all().order_by("name")
    serializer_class = DashboardSerializer
    permission_classes = [permissions.IsAuthenticated]


class EndpointGroupList(APIView):
    """
    List or create epgs
    """

    @swagger_auto_schema(responses={200: EndpointGroupSerializer(many=True)})
    def get(self, request):
        epgs = networks.models.EndpointGroup.objects.all()
        serializer = EndpointGroupSerializer(epgs, many=True)
        return response.Response(serializer.data)

    @swagger_auto_schema(responses={201: EndpointGroupSerializer})
    def post(self, request):
        serializer = EndpointGroupSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return response.Response(serializer.data, status=status.HTTP_201_CREATED)
        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EndpointGroupDetail(APIView):
    """
    Retrieve, update, or delete epgs
    """

    permission_classes = [permissions.IsAuthenticated]

    @swagger_auto_schema(responses={200: EndpointGroupSerializer})
    def get_object(self, id):
        try:
            return networks.models.EndpointGroup.objects.get(id=id)
        except networks.models.EndpointGroup.DoesNotExist:
            raise Http404

    @swagger_auto_schema(responses={200: EndpointGroupSerializer})
    def get(self, request, id):
        epg = self.get_object(id)
        serializer = EndpointGroupSerializer(epg)
        return response.Response(serializer.data)

    @swagger_auto_schema(responses={200: EndpointGroupSerializer})
    def put(self, request, id):
        epg = self.get_object(id)
        serializer = EndpointGroupSerializer(epg, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return response.Response(serializer.data)
        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(responses={200: EndpointGroupSerializer})
    def post(self, request, id):
        epg = self.get_object(id)
        serializer = EndpointGroupSerializer(epg, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return response.Response(serializer.data)
        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema()
    def delete(self, request, id):
        epg = self.get_object(id)
        epg.delete()
        return response.Response(status=status.HTTP_204_NO_CONTENT)


class VlanViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows vlans to be viewed or edited.
    """

    queryset = networks.models.Vlan.objects.all().order_by()
    serializer_class = VlanSerializer
    permission_classes = [permissions.IsAuthenticated]


class InterfaceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows interfaces to be viewed or edited.
    """

    queryset = networks.models.Interface.objects.all().order_by()
    serializer_class = InterfaceSerializer
    permission_classes = [permissions.IsAuthenticated]

"""
class SnapshotViewSet(viewsets.ModelViewSet):
    queryset = networks.models.Snapshot.objects.all().order_by()
    serializer_class = SnapshotSerializer
    permission_classes = [permissions.IsAuthenticated]
"""