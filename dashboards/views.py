import dashboards.models
import utils.helpers
from asgiref.sync import sync_to_async
from django.http import HttpResponseRedirect
from django.views import generic
from django.shortcuts import render
from django_apscheduler.models import DjangoJobExecution
from devices.models import BaseNetworkDevice
from networks.models import BaseNetwork


class Data(generic.View):
    def get(self, request):
        device_executions_count = BaseNetworkDevice.objects.count()
        network_executions_count = BaseNetwork.objects.count()
        job_executions_count = DjangoJobExecution.objects.count()

        return render(request, "dashboards/index.html", {
            "device_count": device_executions_count,
            "network_count": network_executions_count,
            "job_count": job_executions_count,  # Add more variables as needed
        })