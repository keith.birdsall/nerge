from django.db import models
from django_prometheus.models import ExportModelOperationsMixin


class BaseDashboard(ExportModelOperationsMixin("dashboard"), models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, blank=True)
