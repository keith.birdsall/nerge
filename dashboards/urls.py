from django.urls import path
import dashboards.views

app_name = "dashboards"

urlpatterns = [
    path("", dashboards.views.Data.as_view(), name="index"),  # The main index view
]
