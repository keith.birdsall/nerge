import subprocess
import json

container_name = "nsp"  # Replace with your container's name or ID

try:
    # Run the docker inspect command
    result = subprocess.run(
        ["docker", "inspect", "-f", "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}", container_name],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
    )

    if result.returncode == 0:
        ip_address = result.stdout.strip()
        print(f"The IP address of {container_name} is: {ip_address}")

        # Save the IP address to a JSON file
        data = {"container_ip": ip_address}
        with open("mynsp.json", "w") as json_file:
            json.dump(data, json_file)

        print(f"IP address saved to mynsp.json")
    else:
        print(f"Error running docker inspect: {result.stderr}")
except Exception as e:
    print(f"An error occurred: {str(e)}")
