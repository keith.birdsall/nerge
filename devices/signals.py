import devices.models
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
import logging
import utils.helpers

logger = logging.getLogger(__name__)


@receiver(
    post_save,
    sender=devices.models.BaseNetworkDevice,
    dispatch_uid="device_save",
)
def device_save(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("saved device", extra=log_ctx)


@receiver(
    pre_delete,
    sender=devices.models.BaseNetworkDevice,
    dispatch_uid="device_delete",
)
def device_delete(sender, instance, **kwargs):
    instance_dict = utils.helpers.set_dict_vals(instance.__dict__, {"_state": None})
    log_ctx = {"context": instance_dict}
    logger.info("deleting device", extra=log_ctx)
