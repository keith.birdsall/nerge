from django.db import models
from django_prometheus.models import ExportModelOperationsMixin
from django.db.models import JSONField


class BaseNetworkDevice(ExportModelOperationsMixin("device"), models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    network = models.CharField(max_length=50, blank=True)
    salt_key_status = models.CharField(max_length=20, blank=True)
    serial = models.CharField(max_length=100)


class DeviceSnapshot(models.Model):
    id = models.AutoField(primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    devicename = models.CharField(max_length=50, blank=True)
    config = JSONField(default={"run": "run"})