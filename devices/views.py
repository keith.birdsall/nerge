import clients.salt
import devices.models
import utils.helpers
from asgiref.sync import sync_to_async
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import generic
from django.http import HttpResponse
import json
from .models import BaseNetworkDevice



class Index(generic.View):
    async def get(self, request):
        if not await utils.helpers.is_authenticated(request):
            return HttpResponseRedirect("/login/")
        return render(
            request,
            "devices/index.html",
            {
                "devices": await sync_to_async(list)(
                    devices.models.BaseNetworkDevice.objects.all().order_by("name")
                )
            },
        )


class Devices(generic.View):
    async def get(self, request, name=None):
        results = []
        objects = await sync_to_async(list)(
            devices.models.BaseNetworkDevice.objects.filter(name=name)[:1000]
        )
        for object in objects:
            object = object.__dict__
            results.append(object)
        return render(
            request,
            "devices/index.html",
            {"devices": results},
        )


class AcceptDevice(generic.View):
    async def get(self, request, device=None):
        try:
            device_obj = await sync_to_async(
                devices.models.BaseNetworkDevice.objects.get
            )(name=device)
            device_obj.salt_key_status = "accepted"
            await sync_to_async(device_obj.save)()
        except devices.models.BaseNetworkDevice.DoesNotExist:
            raise
        await clients.salt.AsyncClient().accept_key(device)
        return HttpResponseRedirect("/")


class RejectDevice(generic.View):
    async def get(self, request, device=None):
        try:
            device_obj = await sync_to_async(
                devices.models.BaseNetworkDevice.objects.get
            )(name=device)
            device_obj.salt_key_status = "rejected"
            await sync_to_async(device_obj.save)()
        except devices.models.BaseNetworkDevice.DoesNotExist:
            raise
        await clients.salt.AsyncClient().reject_key(device)
        return HttpResponseRedirect("/")


class NetworkDevice(generic.View):
    async def get(self, request, device=None, network=None):
        try:
            device_obj = await sync_to_async(
                devices.models.BaseNetworkDevice.objects.get
            )(name=device)
            device_obj.network = network
            await sync_to_async(device_obj.save)()
        except devices.models.BaseNetworkDevice.DoesNotExist:
            raise
        return HttpResponseRedirect("/")


class DeviceSnapshotSave(generic.View):
    def get(self, request, device):
        try:
            salt_function = clients.salt.Client()
            snap_data = salt_function.snap_config(device)
            start_index = snap_data.find('{')
            json_data = snap_data[start_index:]
            json_data = json.loads(json_data)
            json_data = json.dumps(json_data, indent=4)
            print(json_data)
            if snap_data is None:
                return HttpResponse('SaltStack command failed')
            snap_config_results = devices.models.DeviceSnapshot(
                devicename=device,
                config=json_data,
            )
            snap_config_results.save()
        except devices.models.BaseNetworkDevice.DoesNotExist:
            raise
        return HttpResponse('File copied and saved successfully!')


class DeviceDetails(generic.View):
    def get(self, request):
        hostname = 'leaf01zzz'
        return render(
            request,
            "devices/device_details.html",
            {"hostname": hostname},
        )


class DeviceDetails123(generic.View):
    def get(self, request, device_id):
        # Use the 'device_id' parameter to retrieve the device information
        device = devices.models.BaseNetworkDevice.objects.get(pk=device_id)
        hostname = device.name  # Use the appropriate field here

        return render(
            request,
            "devices/device_details.html",
            {"hostname": hostname},
        )



class DeviceList(generic.View):
    def get(sel, request):
        devices = BaseNetworkDevice.objects.exclude(salt_key_status="unaccepted")
        return render(request, 'devices/device_list.html', {'devices': devices})
