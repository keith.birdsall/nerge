from django.urls import path
import devices.views

app_name = "devices"

urlpatterns = [
    path("", devices.views.Index.as_view(), name="index"),
    path("accept/<str:device>", devices.views.AcceptDevice.as_view(), name="accept"),
    path("reject/<str:device>", devices.views.RejectDevice.as_view(), name="reject"),
    path("devicelist/", devices.views.DeviceList.as_view(), name="devicelist"),
   #path("devicedetails/", devices.views.DeviceDetails.as_view(), name="devicedetails"),
    path("devicedetails/<int:device_id>/", devices.views.DeviceDetails123.as_view(), name="devicedetails"),
    path('snapshot-device/<str:device>/', devices.views.DeviceSnapshotSave.as_view(), name='snapshot-device'),

    path(
        "<str:device>/network/<str:network>",
        devices.views.NetworkDevice.as_view(),
        name="network",
    ),
]
