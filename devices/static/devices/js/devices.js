setActiveBreadcrumbs("devices");
connectWebSocket("devices", uuidv4());
const saltKeyModal = document.getElementById("saltKeyModal");
saltKeyModal.addEventListener("shown.bs.modal", (e) => {
  // Pulls values from html
  const data = Object(e.relatedTarget.dataset);
  const title = data.saltKeyTitle;
  const body = data.saltKeyBody;
  const deviceId = data.deviceId;
  const button = data.saltKeyButton;
  const url = data.saltKeyUrl;

  // Builds the modal
  document.getElementById("saltKeyModalLabel").innerHTML = title;
  document.getElementById("modal-body").innerHTML = body;
  document.getElementById("deviceId").innerHTML = deviceId;
  document.getElementById("salt-key-button").innerHTML = button;
  const link = document.getElementById("results");
  link.setAttribute("href", url);
});
const inputs = document.getElementsByClassName("network-field");

Array.prototype.forEach.call(inputs, function (input) {
  let previousNetwork = null;
  input.addEventListener("focus", function () {
    previousNetwork = this.innerText;
  });
  input.addEventListener("blur", function () {
    const network = this.innerText;
    if (previousNetwork != network) {
      const tableRow = this.parentElement.parentElement;
      const deviceName = tableRow.children[0].innerText;
      try {
        axios.get(`/${deviceName}/network/${network}`, {
          retry: 3,
          retryDelay: 3000,
        });
      } catch (e) {
        console.error(e);
      }
      previousNetwork = network;
    }
  });
});
document.querySelectorAll('.copy-button').forEach(button => {
    button.addEventListener('click', function () {
        const deviceID = button.getAttribute('data-device-id');
        const url = button.getAttribute('data-url');
        copyFile(url, deviceID);
    });
});

function copyFile(url, deviceID) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `${url}?device=${deviceID}`, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            document.getElementById('copy-result').innerHTML = xhr.responseText;
        }
    };
    xhr.send();
}
document.querySelectorAll('.copy-button').forEach(button => {
    const tooltip = button.nextElementSibling;

    button.addEventListener('mouseover', function () {
        tooltip.style.display = 'block';
    });

    button.addEventListener('mouseout', function () {
        tooltip.style.display = 'none';
    });
});

