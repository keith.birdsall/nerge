from django.core.exceptions import PermissionDenied


def allowed_hosts_override(*hosts):
    def decorator(func):
        def wrapper(request, *args, **kwargs):
            print(request.META["HTTP_HOST"])
            if request.META["HTTP_HOST"] in hosts:
                return func(request, *args, **kwargs)
            raise PermissionDenied()

        return wrapper

    return decorator
