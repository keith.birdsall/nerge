import re
import logging

logger = logging.getLogger(__name__)


class LogRequestHeaders:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        regex = re.compile("^HTTP_")
        headers = dict(
            (regex.sub("", header), value)
            for (header, value) in request.META.items()
            if header.startswith("HTTP_")
        )
        logger.info("logging headers", extra={"context": headers})
        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response
