from django.http import JsonResponse
from asgiref.sync import sync_to_async


def json_safe_dict(d):
    if not issubclass(type(d), dict):
        return d
    return dict((json_safe_dict(k), str(v)) for k, v in d.items())


def set_dict_vals(d, map):
    for key, val in map.items():
        if not val:
            try:
                del d[key]
            except KeyError:
                d[key] = val
        else:
            d[key] = val
    return d


def json_response(d, indent=2, sort_keys=False):
    return JsonResponse(
        json_safe_dict(d),
        json_dumps_params={"indent": indent, "sort_keys": sort_keys},
        safe=False,
    )


@sync_to_async
def is_authenticated(request):
    return request.user.is_authenticated if bool(request.user) else None
