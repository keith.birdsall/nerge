APP_NAME ?= nerge
SHA = $(shell git rev-parse --short=8 HEAD)
DOCKER_REPO ?= https://gitlab.com/keith.birdsall/$(APP_NAME)
DOCKER_IMG ?= $(DOCKER_REPO):$(SHA)
DOCKERFILE ?= ./Dockerfile
DOCKER_PLATFORM ?= --platform linux/amd64

.PHONY: all auth build buildx clean dev push

all: build push

default: build

auth:
	docker login -u admin --password admin

clean:
	docker-compose down
	sudo rm -rf .docker-volumes
	sudo docker builder prune -a

nsp:
	docker-compose up -d --remove-orphans --build --force-recreate

build:
	DOCKER_BUILDKIT=0 docker build \
		-f $(DOCKERFILE) \
		--no-cache \
		--build-arg HTTP_PROXY=$(HTTP_PROXY) \
		--build-arg HTTPS_PROXY=$(HTTPS_PROXY) \
		--build-arg NO_PROXY=$(NO_PROXY) \
		-t $(DOCKER_IMG) \
		-t $(DOCKER_REPO):latest .

buildx:
	docker buildx build $(DOCKER_PLATFORM) \
		-f $(DOCKERFILE) \
		--no-cache \
		-t $(DOCKER_IMG) \
		-t $(DOCKER_REPO):latest .

push: auth
	docker push $(DOCKER_REPO):latest
	docker push $(DOCKER_IMG)
