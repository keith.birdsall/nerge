# NSP-Light
Network Sciences Platform
This is a full demo deployment for show and development.
Running this will install 1 container and three vm's within the
container.

### System Requirements
- Mac OS
- Intell or M1 chipset
- Python 3.8 or later
- Any choice or your dev sdk just as PyCharm / Visual studio or more.
- Note: This can operate on LINUX based systems a well

### Install Instructions
Connect and git the REPO
In the directory of your git download run
- sudo make -B nsp

This will download docker images and install the libraries and more
Note: can take a few minutes to deploy. After complete you will need to run
a few more commands to get the container up and running
- python3 manage.py collectstatic
- python3 manage.py createsuperuser

Follow the onscreen prompts to add the user and password to the NSP app
- python3 manage.py migrate

Next you will need to start up the NSP application run the below command
#### Running the application
- sudo SCHEDULER_AUTOSTART=True python3 -m gunicorn --bind 0.0.0.0:8000 --preload nsp.asgi:application -k uvicorn.workers.UvicornWorker -w 4
